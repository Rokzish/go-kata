package task

import (
	"errors"
	"os"
	"reflect"
	"testing"

	"github.com/brianvoe/gofakeit/v6"
)

var pathJSON, _ = os.Getwd()

func TestLoadData(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		path string
	}

	var (
		tmpCommit, _ = generateCommitForTests()
		testNode1    = &Node{
			data: tmpCommit,
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: tmpCommit,
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: tmpCommit,
			prev: nil,
			next: nil,
		}
	)
	testNode1.data = tmpCommit

	testNode2.next, testNode3.prev = testNode3, testNode2

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test 1, empty List, correct path",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args:    args{path: pathJSON + "/test.json"},
			wantErr: false,
		},
		{
			name: "test 2, 1 length List, correct path",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			args:    args{path: pathJSON + "/test.json"},
			wantErr: false,
		},
		{
			name: "test 3, empty List, incorrect path",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			args:    args{path: ""},
			wantErr: true,
		},
		{
			name: "test 4, 1 length List, incorrect path",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			args:    args{path: ""},
			wantErr: true,
		},
		{
			name: "test 5, >1 length List, correct path ",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			args:    args{path: pathJSON + "/test.json"},
			wantErr: false,
		},
		{
			name: "test 6, >1 length List, incorrect path",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			args:    args{path: ""},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			err := d.LoadData(tt.args.path)
			if (err != nil) != tt.wantErr {
				t.Errorf("LoadData() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil {
				i := 0
				for currNode := d.head; currNode.next != nil; currNode = currNode.next {
					if !currNode.data.Date.Before(currNode.next.data.Date) && !currNode.data.Date.Equal(currNode.next.data.Date) {
						t.Error("LoadData() error = incorrect sorting")
					}
					i++
				}
			}

		})
	}
}

func TestLen(t *testing.T) {
	type fields struct {
		len int
	}
	tests := []struct {
		name   string
		fields fields
		want   int
	}{
		{
			name: "test 1, 0 length List",
			fields: fields{
				len: 0,
			},
			want: 0,
		},
		{
			name: "test 2, 1 length List",
			fields: fields{
				len: 1,
			},
			want: 1,
		},
		{
			name: "test 3, >1 length List",
			fields: fields{
				len: 2,
			},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				len: tt.fields.len,
			}
			if got := d.Len(); got != tt.want {
				t.Errorf("Len() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCurrent(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}

	var (
		testNode1 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
	)
	testNode2.next, testNode3.prev = testNode3, testNode2
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "test 1, 1 length List, with current node",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			want: testNode1,
		},
		{
			name: "test 2, >1 length List. with current node",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			want: testNode2,
		},
		{
			name: "test 3, >1 length List. with current node",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode3,
				len:  2,
			},
			want: testNode3,
		},
		{
			name: "test 4. >1 length List. without current node",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			want: nil,
		},
		{
			name: "test 5, 0 length List, without current node",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Current(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Current() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNext(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testNode1 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
	)
	testNode2.next, testNode3.prev = testNode3, testNode2
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "test 1, 1 length List",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			want: nil,
		},
		{
			name: "test 2, >1 length List, first node",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			want: testNode3,
		},
		{
			name: "test 3, >1 length List, second node",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode3,
				len:  2,
			},
			want: nil,
		},
		{
			name: "test 4, >1 length List, without current",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Next(); !reflect.DeepEqual(got, tt.want) || !reflect.DeepEqual(d.curr, tt.want) {
				t.Errorf("Next() = %v, d.curr = %v,  want = %v", got, d.curr, tt.want)
			}
		})
	}
}

func TestPrev(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testNode1 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
	)
	testNode2.next, testNode3.prev = testNode3, testNode2
	tests := []struct {
		name   string
		fields fields
		want   *Node
	}{
		{
			name: "test 1, 1 length List",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			want: nil,
		},
		{
			name: "test 2, >1 length List, first node",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			want: nil,
		},
		{
			name: "test 3, >1 length List, second node",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode3,
				len:  2,
			},
			want: testNode2,
		},
		{
			name: "test 4, >1 length List, without current",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Prev(); !reflect.DeepEqual(got, tt.want) || !reflect.DeepEqual(d.curr, tt.want) {
				t.Errorf("Prev() = %v, d.curr = %v,  want = %v", got, d.curr, tt.want)
			}
		})
	}
}

func TestInsert(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
		c Commit
	}
	var (
		tmpCommit, _ = generateCommitForTests()
		testNode1    = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
	)
	testNode2.next, testNode3.prev = testNode3, testNode2
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test 1, 1 length List",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			args: args{
				n: 0,
				c: *tmpCommit,
			},
			wantErr: false,
		},
		{
			name: "test 2, 1 length List, incorrect index",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			args: args{
				n: 1,
				c: *tmpCommit,
			},
			wantErr: true,
		},
		{
			name: "test 3, >1 length List, correct index",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			args: args{
				n: 0,
				c: *tmpCommit,
			},
			wantErr: false,
		},
		{
			name: "test 4, >1 length List, correct index",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			args: args{
				n: 1,
				c: *tmpCommit,
			},
			wantErr: false,
		},
		{
			name: "test 5, >1 length List, incorrect index",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			args: args{
				n: 2,
				c: *tmpCommit,
			},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			err := d.Insert(tt.args.n, tt.args.c)
			if (err != nil) != tt.wantErr {
				t.Errorf("Insert() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil {
				currNode := d.head
				for i := 0; i <= tt.args.n; i++ {
					currNode = currNode.next
				}
				if *currNode.data != tt.args.c {
					t.Errorf("Insert() = %v, want %v", *currNode.data, tt.args.c)
				}
			}
		})
	}
}

func TestDelete(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	type args struct {
		n int
	}
	var (
		testNode1 = &Node{
			data: &Commit{Message: "1"},
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: &Commit{Message: "2"},
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: &Commit{Message: "3"},
			prev: nil,
			next: nil,
		}
		testNode4 = &Node{
			data: &Commit{Message: "4"},
			prev: nil,
			next: nil,
		}
		testNode5 = &Node{
			data: &Commit{Message: "5"},
			prev: nil,
			next: nil,
		}
		testNode6 = &Node{
			data: &Commit{Message: "6"},
			prev: nil,
			next: nil,
		}
		testNode7 = &Node{
			data: &Commit{Message: "7"},
			prev: nil,
			next: nil,
		}
		testNode8 = &Node{
			data: &Commit{Message: "8"},
			prev: nil,
			next: nil,
		}
		testNode9 = &Node{
			data: &Commit{Message: "9"},
			prev: nil,
			next: nil,
		}
	)
	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5

	testNode7.next, testNode8.prev = testNode8, testNode7
	testNode8.next, testNode9.prev = testNode9, testNode8

	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test 1, 1 length List, correct index",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			args:    args{n: 0},
			wantErr: false,
		},
		{
			name: "test 2, 1 length List, incorrect index",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			args:    args{n: 1},
			wantErr: true,
		},
		{
			name: "test 3, 1 length List, incorrect index",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			args:    args{n: -1},
			wantErr: true,
		},
		{
			name: "test 4, >1 length List, first index",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			args:    args{n: 0},
			wantErr: false,
		},
		{
			name: "test 5, >1 length List, second index",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: testNode4,
				len:  3,
			},
			args:    args{n: 1},
			wantErr: false,
		},
		{
			name: "test 6, >1 length List, last index",
			fields: fields{
				head: testNode7,
				tail: testNode9,
				curr: testNode7,
				len:  3,
			},
			args:    args{n: 2},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			err := d.Delete(tt.args.n)
			if (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}

		})
	}
}

func TestDeleteCurrent(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testNode1 = &Node{
			data: &Commit{Message: "1"},
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: &Commit{Message: "2"},
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: &Commit{Message: "3"},
			prev: nil,
			next: nil,
		}
		testNode4 = &Node{
			data: &Commit{Message: "4"},
			prev: nil,
			next: nil,
		}
		testNode5 = &Node{
			data: &Commit{Message: "5"},
			prev: nil,
			next: nil,
		}
		testNode6 = &Node{
			data: &Commit{Message: "6"},
			prev: nil,
			next: nil,
		}
	)
	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5

	tests := []struct {
		name    string
		fields  fields
		wantErr bool
	}{
		{
			name: "test 1, 1 length List, with current",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			wantErr: false,
		},
		{
			name: "test 2, 1 length List, without current",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			wantErr: true,
		},
		{
			name: "test 3, >1 length List, with current",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			wantErr: false,
		},
		{
			name: "test 4, >1 length List, with current",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: testNode5,
				len:  3,
			},
			wantErr: false,
		},
		{
			name: "test 5, >1 length List, with current",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: testNode6,
				len:  3,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if err := d.DeleteCurrent(); (err != nil) != tt.wantErr {
				t.Errorf("DeleteCurrent() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestIndex(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testNode1 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode2 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode3 = &Node{
			data: nil,
			prev: nil,
			next: nil,
		}
		testNode4 = &Node{
			data: &Commit{Message: "4"},
			prev: nil,
			next: nil,
		}
		testNode5 = &Node{
			data: &Commit{Message: "5"},
			prev: nil,
			next: nil,
		}
		testNode6 = &Node{
			data: &Commit{Message: "6"},
			prev: nil,
			next: nil,
		}
	)

	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5

	tests := []struct {
		name    string
		fields  fields
		want    int
		wantErr bool
	}{
		{
			name: "test 1, 1 length List, with current",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			want:    0,
			wantErr: false,
		},
		{
			name: "test 2, 1 length List, without current",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			want:    -1,
			wantErr: true,
		},
		{
			name: "test 3, >1 length List, with current",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode3,
				len:  2,
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "test 4, >1 length List, with current",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: testNode5,
				len:  3,
			},
			want:    1,
			wantErr: false,
		},
		{
			name: "test 4, 1 length List, without current",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: nil,
				len:  3,
			},
			want:    -1,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			got, err := d.Index()
			if (err != nil) != tt.wantErr {
				t.Errorf("Index() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("Index() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPop(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testData1, _ = generateCommitForTests()
		testNode1    = &Node{
			data: testData1,
			prev: nil,
			next: nil,
		}
		testData2, _ = generateCommitForTests()
		testNode2    = &Node{
			data: testData2,
			prev: nil,
			next: nil,
		}
		testData3, _ = generateCommitForTests()
		testNode3    = &Node{
			data: testData3,
			prev: nil,
			next: nil,
		}
		testData4, _ = generateCommitForTests()
		testNode4    = &Node{
			data: testData4,
			prev: nil,
			next: nil,
		}
		testData5, _ = generateCommitForTests()
		testNode5    = &Node{
			data: testData5,
			prev: nil,
			next: nil,
		}
		testData6, _ = generateCommitForTests()
		testNode6    = &Node{
			data: testData6,
			prev: nil,
			next: nil,
		}
	)

	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5

	tests := []struct {
		name    string
		fields  fields
		want    *Node
		wantLen int
	}{
		{
			name: "test 1, 1 length List",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			want: &Node{
				data: testData1,
				prev: nil,
				next: nil,
			},
			wantLen: 0,
		},
		{
			name: "test 2. >1 length List",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			want: &Node{
				data: testData3,
				prev: nil,
				next: nil,
			},
			wantLen: 1,
		},
		{
			name: "test 3, >1 length List",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: testNode4,
				len:  3,
			},
			want: &Node{
				data: testData6,
				prev: nil,
				next: nil,
			},
			wantLen: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Pop(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Pop() = %v, want %v", got, tt.want)
			}
			if d.len != tt.wantLen {
				t.Errorf("List length = %d, want = %d", d.len, tt.wantLen)
			}
		})
	}
}

func TestShift(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testData1, _ = generateCommitForTests()
		testNode1    = &Node{
			data: testData1,
			prev: nil,
			next: nil,
		}
		testData2, _ = generateCommitForTests()
		testNode2    = &Node{
			data: testData2,
			prev: nil,
			next: nil,
		}
		testData3, _ = generateCommitForTests()
		testNode3    = &Node{
			data: testData3,
			prev: nil,
			next: nil,
		}
		testData4, _ = generateCommitForTests()
		testNode4    = &Node{
			data: testData4,
			prev: nil,
			next: nil,
		}
		testData5, _ = generateCommitForTests()
		testNode5    = &Node{
			data: testData5,
			prev: nil,
			next: nil,
		}
		testData6, _ = generateCommitForTests()
		testNode6    = &Node{
			data: testData6,
			prev: nil,
			next: nil,
		}
	)

	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5

	tests := []struct {
		name    string
		fields  fields
		want    *Node
		wantLen int
	}{
		{
			name: "test 1, 1 length List",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			want: &Node{
				data: testData1,
				prev: nil,
				next: nil,
			},
			wantLen: 0,
		},
		{
			name: "test 2. >1 length List",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			want: &Node{
				data: testData2,
				prev: nil,
				next: nil,
			},
			wantLen: 1,
		},
		{
			name: "test 3, >1 length List",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: testNode4,
				len:  3,
			},
			want: &Node{
				data: testData4,
				prev: nil,
				next: nil,
			},
			wantLen: 2,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Shift(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Shift() = %v, want %v", got, tt.want)
			}
			if d.len != tt.wantLen {
				t.Errorf("List length = %d, want = %d", d.len, tt.wantLen)
			}
		})
	}
}

func TestSearch(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testData1, _ = generateCommitForTests()
		testNode1    = &Node{
			data: testData1,
			prev: nil,
			next: nil,
		}
		testData2, _ = generateCommitForTests()
		testNode2    = &Node{
			data: testData2,
			prev: nil,
			next: nil,
		}
		testData3, _ = generateCommitForTests()
		testNode3    = &Node{
			data: testData3,
			prev: nil,
			next: nil,
		}
		testData4, _ = generateCommitForTests()
		testNode4    = &Node{
			data: testData4,
			prev: nil,
			next: nil,
		}
		testData5, _ = generateCommitForTests()
		testNode5    = &Node{
			data: testData5,
			prev: nil,
			next: nil,
		}
		testData6, _ = generateCommitForTests()
		testNode6    = &Node{
			data: testData6,
			prev: nil,
			next: nil,
		}
	)

	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5
	type args struct {
		message string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name: "test 1, 1 length List, correct message",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			args: args{message: testData1.Message},
			want: testNode1,
		},
		{
			name: "test 2, 1 length List, incorrect message",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			args: args{message: testData2.Message},
			want: nil,
		},
		{
			name: "test 3, >1 length List, correct message",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			args: args{message: testData3.Message},
			want: testNode3,
		},
		{
			name: "test 4, >1 length List, incorrect message",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			args: args{message: testData4.Message},
			want: nil,
		},
		{
			name: "test 5, >1 length List, correct message",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: nil,
				len:  3,
			},
			args: args{message: testData5.Message},
			want: testNode5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Search(tt.args.message); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Search() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestSearchUUID(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	var (
		testData1, _ = generateCommitForTests()
		testNode1    = &Node{
			data: testData1,
			prev: nil,
			next: nil,
		}
		testData2, _ = generateCommitForTests()
		testNode2    = &Node{
			data: testData2,
			prev: nil,
			next: nil,
		}
		testData3, _ = generateCommitForTests()
		testNode3    = &Node{
			data: testData3,
			prev: nil,
			next: nil,
		}
		testData4, _ = generateCommitForTests()
		testNode4    = &Node{
			data: testData4,
			prev: nil,
			next: nil,
		}
		testData5, _ = generateCommitForTests()
		testNode5    = &Node{
			data: testData5,
			prev: nil,
			next: nil,
		}
		testData6, _ = generateCommitForTests()
		testNode6    = &Node{
			data: testData6,
			prev: nil,
			next: nil,
		}
	)

	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5

	type args struct {
		uuID string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   *Node
	}{
		{
			name: "test 1, 1 length List, correct uuID",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			args: args{uuID: testData1.UUID},
			want: testNode1,
		},
		{
			name: "test 2, 1 length List, incorrect uuID",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: nil,
				len:  1,
			},
			args: args{uuID: testData2.UUID},
			want: nil,
		},
		{
			name: "test 3, 1 length List, correct uuId",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			args: args{uuID: testData3.UUID},
			want: testNode3,
		},
		{
			name: "test 4, 1 length List, incorrect uuId",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: nil,
				len:  2,
			},
			args: args{uuID: testData1.UUID},
			want: nil,
		},
		{
			name: "test 5, 1 length List, correct uuId",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: nil,
				len:  3,
			},
			args: args{uuID: testData5.UUID},
			want: testNode5,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.SearchUUID(tt.args.uuID); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("SearchUUID() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestReverse(t *testing.T) {
	type fields struct {
		head *Node
		tail *Node
		curr *Node
		len  int
	}
	// data for tests
	var (
		testData1, _ = generateCommitForTests()
		testNode1    = &Node{
			data: testData1,
			prev: nil,
			next: nil,
		}
		testData2, _ = generateCommitForTests()
		testNode2    = &Node{
			data: testData2,
			prev: nil,
			next: nil,
		}
		testData3, _ = generateCommitForTests()
		testNode3    = &Node{
			data: testData3,
			prev: nil,
			next: nil,
		}
		testData4, _ = generateCommitForTests()
		testNode4    = &Node{
			data: testData4,
			prev: nil,
			next: nil,
		}
		testData5, _ = generateCommitForTests()
		testNode5    = &Node{
			data: testData5,
			prev: nil,
			next: nil,
		}
		testData6, _ = generateCommitForTests()
		testNode6    = &Node{
			data: testData6,
			prev: nil,
			next: nil,
		}
	)

	testNode2.next, testNode3.prev = testNode3, testNode2

	testNode4.next, testNode5.prev = testNode5, testNode4
	testNode5.next, testNode6.prev = testNode6, testNode5

	// data for wanted result
	var (
		wantedNode1 = &Node{
			data: testData1,
			prev: nil,
			next: nil,
		}
		wantedNode2 = &Node{
			data: testData2,
			prev: nil,
			next: nil,
		}
		wantedNode3 = &Node{
			data: testData3,
			prev: nil,
			next: nil,
		}
		wantedNode4 = &Node{
			data: testData4,
			prev: nil,
			next: nil,
		}
		wantedNode5 = &Node{
			data: testData5,
			prev: nil,
			next: nil,
		}
		wantedNode6 = &Node{
			data: testData6,
			prev: nil,
			next: nil,
		}
	)

	wantedNode3.next, wantedNode2.prev = wantedNode2, wantedNode3

	wantedNode6.next, wantedNode5.prev = wantedNode5, wantedNode6
	wantedNode5.next, wantedNode4.prev = wantedNode4, wantedNode5

	tests := []struct {
		name   string
		fields fields
		want   *DoubleLinkedList
	}{
		{
			name: "test 1, 1 length List",
			fields: fields{
				head: testNode1,
				tail: testNode1,
				curr: testNode1,
				len:  1,
			},
			want: &DoubleLinkedList{
				head: wantedNode1,
				tail: wantedNode1,
				curr: wantedNode1,
				len:  1,
			},
		},
		{
			name: "test 2, >1 length List",
			fields: fields{
				head: testNode2,
				tail: testNode3,
				curr: testNode2,
				len:  2,
			},
			want: &DoubleLinkedList{
				head: wantedNode3,
				tail: wantedNode2,
				curr: wantedNode2,
				len:  2,
			},
		},
		{
			name: "test 3, >1 length List",
			fields: fields{
				head: testNode4,
				tail: testNode6,
				curr: testNode4,
				len:  3,
			},
			want: &DoubleLinkedList{
				head: wantedNode6,
				tail: wantedNode4,
				curr: wantedNode4,
				len:  3,
			},
		},
		{
			name: "test 4, 0 length List",
			fields: fields{
				head: nil,
				tail: nil,
				curr: nil,
				len:  0,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			d := &DoubleLinkedList{
				head: tt.fields.head,
				tail: tt.fields.tail,
				curr: tt.fields.curr,
				len:  tt.fields.len,
			}
			if got := d.Reverse(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func generateList(len int) (*DoubleLinkedList, error) {
	if len < 0 {
		return nil, errors.New("invalid length value")
	}
	if len == 0 {
		return &DoubleLinkedList{head: nil, tail: nil, curr: nil, len: 0}, nil
	}

	firstNode := &Node{}
	firstNode.data, _ = generateCommitForTests()

	newList := &DoubleLinkedList{
		head: firstNode,
		tail: firstNode,
		curr: firstNode,
		len:  1,
	}

	for i := 0; i < len; i++ {
		data, _ := generateCommitForTests()
		err := newList.Insert(i, *data)
		if err != nil {
			return nil, err
		}
	}

	return newList, nil

}

func BenchmarkDoubleLinkedList_LoadData(b *testing.B) {

	var generatedLists []*DoubleLinkedList
	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].LoadData(pathJSON + "/test.json")
	}

}

func BenchmarkDoubleLinkedList_Len(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		generatedLists[i].Len()

	}
}

func BenchmarkDoubleLinkedList_Prev(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		list.curr = list.tail
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Len()
	}
}

func BenchmarkDoubleLinkedList_Current(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		list.curr = list.head
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Len()
	}
}

func BenchmarkDoubleLinkedList_Next(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		list.curr = list.head
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Len()
	}
}

func BenchmarkDoubleLinkedList_Insert(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		generatedLists = append(generatedLists, list)
	}
	var generatedCommits []Commit
	for i := 0; i < b.N; i++ {
		commit, _ := generateCommitForTests()
		generatedCommits = append(generatedCommits, *commit)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Insert(9, generatedCommits[i])
	}
}

func BenchmarkDoubleLinkedList_Delete(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Delete(9)
	}

}

func BenchmarkDoubleLinkedList_DeleteCurrent(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		list.curr = list.tail
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].DeleteCurrent()
	}
}

func BenchmarkDoubleLinkedList_Index(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		list.curr = list.tail
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_, _ = generatedLists[i].Index()
	}
}

func BenchmarkDoubleLinkedList_Pop(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Pop()
	}
}

func BenchmarkDoubleLinkedList_Shift(b *testing.B) {
	var generatedLists []*DoubleLinkedList

	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Shift()
	}
}

func BenchmarkDoubleLinkedList_Search(b *testing.B) {
	var generatedLists []*DoubleLinkedList
	var searchedMessages []string
	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		searchedMessages = append(searchedMessages, list.tail.data.Message)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Search(searchedMessages[i])
	}
}

func BenchmarkDoubleLinkedList_SearchUUID(b *testing.B) {
	var generatedLists []*DoubleLinkedList
	var searchedUUIDs []string
	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		searchedUUIDs = append(searchedUUIDs, list.tail.data.UUID)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Search(searchedUUIDs[i])
	}
}

func BenchmarkDoubleLinkedList_Reverse(b *testing.B) {
	var generatedLists []*DoubleLinkedList
	for i := 0; i < b.N; i++ {
		list, _ := generateList(10)
		generatedLists = append(generatedLists, list)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = generatedLists[i].Reverse()
	}
}

// generateCommitForTests возвращает данные типа Commit, сгенерированные при помощи gofakeit
func generateCommitForTests() (*Commit, error) {

	newCommit := &Commit{}
	err := gofakeit.Struct(newCommit)
	if err != nil {
		return nil, err
	}

	return newCommit, nil
}
