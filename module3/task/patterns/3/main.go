package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

// WeatherAPI is the interface that defines the methods for accessing weather information
type WeatherAPI interface {
	GetTemperature(location string) int
	GetHumidity(location string) int
	GetWindSpeed(location string) int
}

// OpenWeatherAPI is the implementation of the weather API
type OpenWeatherAPI struct {
	apiKey string
}

func (o *OpenWeatherAPI) GetTemperature(location string) int {
	// Make a request to the open weather API to retrieve temperature information
	// and return the result
	// ...
	var city string
	switch location {
	case "Москва":
		city = "lat=55.755773&lon=37.617761"
	case "Санкт-Петербуг":
		city = "lat=59.938806&lon=30.314278"
	case "Казань":
		city = "lat=55.795793&lon=49.106585"
	case "Якутск":
		city = "lat=62.027833&lon=129.704151"
	default:
		log.Println("Wrong location")
		return 0
	}
	url := "https://api.weather.yandex.ru/v2/informers?" + city

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}

	req.Header.Add("X-Yandex-API-Key", o.apiKey)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		log.Println("Error on response.\n[ERROR] -", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error while reading the response bytes:", err)
	}

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		panic(err)
	}

	val := data.(map[string]interface{})["fact"].(map[string]interface{})["temp"]
	temp := int(val.(float64))
	return temp
}

func (o *OpenWeatherAPI) GetHumidity(location string) int {
	// Make a request to the open weather API to retrieve humidity information
	// and return the result
	// ...
	var city string
	switch location {
	case "Москва":
		city = "lat=55.755773&lon=37.617761"
	case "Санкт-Петербуг":
		city = "lat=59.938806&lon=30.314278"
	case "Казань":
		city = "lat=55.795793&lon=49.106585"
	case "Якутск":
		city = "lat=62.027833&lon=129.704151"
	default:
		log.Println("Wrong location")
		return 0
	}
	url := "https://api.weather.yandex.ru/v2/informers?" + city

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}

	req.Header.Add("X-Yandex-API-Key", o.apiKey)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		log.Println("Error on response.\n[ERROR] -", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error while reading the response bytes:", err)
	}

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		panic(err)
	}
	val := data.(map[string]interface{})["fact"].(map[string]interface{})["humidity"]
	hum := int(val.(float64))
	return hum
}

func (o *OpenWeatherAPI) GetWindSpeed(location string) int {
	// Make a request to the open weather API to retrieve wind speed information
	// and return the result
	// ...
	var city string
	switch location {
	case "Москва":
		city = "lat=55.755773&lon=37.617761"
	case "Санкт-Петербуг":
		city = "lat=59.938806&lon=30.314278"
	case "Казань":
		city = "lat=55.795793&lon=49.106585"
	case "Якутск":
		city = "lat=62.027833&lon=129.704151"
	default:
		log.Println("Wrong location")
		return 0
	}
	url := "https://api.weather.yandex.ru/v2/informers?" + city

	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		panic(err)
	}

	req.Header.Add("X-Yandex-API-Key", o.apiKey)

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		log.Println("Error on response.\n[ERROR] -", err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Println("Error while reading the response bytes:", err)
	}

	var data interface{}
	err = json.Unmarshal(body, &data)
	if err != nil {
		panic(err)
	}
	val := data.(map[string]interface{})["fact"].(map[string]interface{})["wind_speed"]
	wind := int(val.(float64))
	return wind
}

// WeatherFacade is the facade that provides a simplified interface to the weather API
type WeatherFacade struct {
	weatherAPI WeatherAPI
}

func (w *WeatherFacade) GetWeatherInfo(location string) (int, int, int) {
	temperature := w.weatherAPI.GetTemperature(location)
	humidity := w.weatherAPI.GetHumidity(location)
	windSpeed := w.weatherAPI.GetWindSpeed(location)

	return temperature, humidity, windSpeed
}

func NewWeatherFacade(apiKey string) WeatherFacade {
	return WeatherFacade{
		weatherAPI: &OpenWeatherAPI{apiKey: apiKey},
	}
}

func main() {
	weatherFacade := NewWeatherFacade("f2ec4a4d-c78e-49d2-bbb6-5859a9929d68")
	cities := []string{"Москва", "Санкт-Петербуг", "Казань", "Якутск"}

	for _, city := range cities {
		temperature, humidity, windSpeed := weatherFacade.GetWeatherInfo(city)
		fmt.Printf("Temperature in "+city+": %d\n", temperature)
		fmt.Printf("Humidity in "+city+": %d\n", humidity)
		fmt.Printf("Wind speed in "+city+": %d\n\n", windSpeed)
	}

}
