package main

import "fmt"

type PricingStrategy interface {
	Calculate(order Order) float64
}

type RegularPricing struct {
}

func (rp RegularPricing) Calculate(order Order) float64 {
	return order.price * order.count
}

type SalePricing struct {
	saleName string
}

func (sp SalePricing) Calculate(order Order) float64 {
	var discount float64
	switch sp.saleName {
	case "weekly sale":
		discount = .95
	case "1+1":
		discount = .9
	case "liquidation":
		discount = .75
	case "loyalty card":
		discount = .93
	}

	return order.price * discount * order.count
}

type Order struct {
	name  string
	price float64
	count float64
}

func main() {
	strategies := []PricingStrategy{
		RegularPricing{},
		SalePricing{saleName: "1+1"},
		SalePricing{saleName: "loyalty card"},
		SalePricing{saleName: "liquidation"},
		SalePricing{saleName: "weekly sale"},
	}
	products := []Order{
		{"bottle of water", 10, 3},
		{"wine", 100, 2},
		{"meat", 50, 1.2},
		{"rice", 30, 5},
		{"apples", 15, .6},
	}

	for i := 0; i < 5; i++ {
		fmt.Printf("Total cost of %s with %T: %.2f\n", products[i].name, strategies[i], strategies[i].Calculate(products[i]))
	}
}
