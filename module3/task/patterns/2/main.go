package main

import "fmt"

type AirConditioner interface {
	TurnOn()
	TurnOff()
	SetTemperature(temp int)
}

type RealAirConditioner struct {
	turn        bool
	temperature float64
}

func (rc *RealAirConditioner) TurnOn() {
	fmt.Println("Turning on the air conditioner")
	rc.turn = true
}
func (rc *RealAirConditioner) TurnOff() {
	fmt.Println("Turning off the air conditioner")
	rc.turn = false
}

func (rc *RealAirConditioner) SetTemperature(temp float64) {
	fmt.Println("Setting air condition temperature to", temp)
	rc.temperature = temp
}

type AirConditionerAdapter struct {
	airConditioner *RealAirConditioner
}

func (a *AirConditionerAdapter) TurnOn() {
	a.airConditioner.TurnOn()
}

func (a *AirConditionerAdapter) TurnOff() {
	a.airConditioner.TurnOff()
}

func (a *AirConditionerAdapter) SetTemperature(temp float64) {
	a.airConditioner.SetTemperature(temp)
}

type AirConditionerProxy struct {
	adapter       *AirConditionerAdapter
	authenticated bool
}

func (p *AirConditionerProxy) TurnOn() {
	if !p.checkAuth() {
		fmt.Println("authenticated required to turn on the air conditioner")
		return
	}
	p.adapter.TurnOn()
}
func (p *AirConditionerProxy) TurnOff() {
	if !p.checkAuth() {
		fmt.Println("authenticated required to turn off the air conditioner")
		return
	}
	p.adapter.TurnOff()
}

func (p *AirConditionerProxy) SetTemperature(temp float64) {
	if !p.checkAuth() {
		fmt.Println("authenticated required to ser temperature the air conditioner")
		return
	}
	p.adapter.SetTemperature(temp)
}

func (p *AirConditionerProxy) checkAuth() bool {
	if !p.authenticated {
		fmt.Print("Access denied: ")
		return false
	}
	return true
}

func NewAirConditionerProxy(authenticated bool) *AirConditionerProxy {
	return &AirConditionerProxy{
		adapter:       &AirConditionerAdapter{airConditioner: &RealAirConditioner{}},
		authenticated: authenticated,
	}
}

func main() {
	airConditioner := NewAirConditionerProxy(false) // without auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)

	airConditioner = NewAirConditionerProxy(true) // with auth
	airConditioner.TurnOn()
	airConditioner.TurnOff()
	airConditioner.SetTemperature(25)
}
