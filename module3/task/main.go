package task

import (
	"encoding/json"
	"errors"
	"io"
	"math/rand"
	"os"
	"time"

	"github.com/brianvoe/gofakeit/v6"
)

type DoubleLinkedList struct {
	head *Node // начальный элемент в списке
	tail *Node // последний элемент в списке
	curr *Node // текущий элемент меняется при использовании методов next, prev
	len  int   // количество элементов в списке
}

type LinkedLister interface {
	Len() int
	Current() *Node
	Next() *Node
	Prev() *Node
	LoadData(path string) error
	Insert(n int, c Commit) error
	Delete(n int) error
	DeleteCurrent() error
	Index() error
	Pop() *Node
	Shift() *Node
	Search(message string) *Node
	SearchUUID(uuID string) *Node
	Reverse() *DoubleLinkedList
}

// LoadData загрузка данных из подготовленного json файла
func (d *DoubleLinkedList) LoadData(path string) error {
	// отсортировать список используя самописный QuickSort
	f, err := os.Open(path)
	if err != nil {
		return err
	}
	defer f.Close()

	b := make([]byte, 1024)
	var data []byte
	for {
		n, err := f.Read(b)
		b = b[:n]
		if err != nil {
			if err == io.EOF {
				break
			} else {
				return err
			}
		}
		data = append(data, b...)
	}

	var unmarshalledData []Commit
	err = json.Unmarshal(data, &unmarshalledData)
	if err != nil {
		return err
	}

	var commitsToSort []Commit
	commitsToSort = append(commitsToSort, unmarshalledData...)

	for n := d.head; n != nil; n = n.next {
		commitsToSort = append(commitsToSort, *n.data)
	}

	sortedCommits := quickSortCommits(commitsToSort)

	newList := &DoubleLinkedList{
		head: nil,
		tail: nil,
		curr: nil,
		len:  0,
	}
	i := 0

	firstNode := &Node{
		data: &sortedCommits[0],
		prev: nil,
		next: nil,
	}
	newList.len++
	newList.head = firstNode
	newList.tail = firstNode
	newList.curr = firstNode
	i = 1

	for ; i < len(sortedCommits); i++ {
		err = newList.Insert(newList.Len()-1, sortedCommits[i])
		if err != nil {
			return err
		}
	}

	*d = *newList

	return nil
}

// Len получение длины списка
func (d *DoubleLinkedList) Len() int {
	if d == nil {
		return -1
	}
	return d.len
}

// Current получение текущего элемента
func (d *DoubleLinkedList) Current() *Node {
	return d.curr
}

// Next получение следующего элемента
func (d *DoubleLinkedList) Next() *Node {
	if d.curr == nil {
		return nil
	}
	d.curr = d.curr.next
	return d.curr
}

// Prev получение предыдущего элемента
func (d *DoubleLinkedList) Prev() *Node {
	if d.curr == nil {
		return nil
	}
	d.curr = d.curr.prev
	return d.curr
}

// Insert вставка элемента после n элемента
func (d *DoubleLinkedList) Insert(n int, c Commit) error {
	if d.Len() <= 0 {
		return errors.New("the list is empty, operation is not available")
	}
	if d.Len()-1 < n || n < 0 {
		return errors.New("index out of range")
	}
	newNode := &Node{
		data: &c,
		prev: nil,
		next: nil,
	}

	idx, err := d.Index()
	if err != nil {
		return err
	}
	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}
	currNode := d.Current()
	if currNode.next == nil {
		currNode.next, newNode.prev = newNode, currNode
		d.tail = newNode
	} else {
		nextNode := currNode.next
		currNode.next = newNode
		newNode.prev = currNode
		nextNode.prev = newNode
		newNode.next = nextNode
	}
	d.len++
	d.Next()
	return nil
}

// Delete удаление n элемента
func (d *DoubleLinkedList) Delete(n int) error {
	if d.Len() < 1 {
		return errors.New("the list is empty, operation is not available")
	}
	if n > d.Len()-1 || n < 0 {
		return errors.New("index out of range")
	}
	idx, err := d.Index()
	if err != nil {
		return err
	}
	if idx > n {
		for i := 0; i < idx-n; i++ {
			d.Prev()
		}
	} else {
		for i := 0; i < n-idx; i++ {
			d.Next()
		}
	}
	_ = d.DeleteCurrent()

	return nil
}

// DeleteCurrent удаление текущего элемента
func (d *DoubleLinkedList) DeleteCurrent() error {
	if d.curr == nil {
		return errors.New("current node is missing")
	}
	if d.Len() == 1 {
		d.head, d.tail = nil, nil
		d.len--
		return nil
	}
	nextNode := d.Current().next

	prevNode := d.Current().prev

	if nextNode == nil {
		d.tail = prevNode
		prevNode.next = nil
	} else if prevNode == nil {
		d.head = nextNode
		nextNode.prev = nil
	} else {
		prevNode.next, nextNode.prev = nextNode, prevNode
	}
	d.len--
	return nil
}

// Index получение индекса текущего элемента
func (d *DoubleLinkedList) Index() (int, error) {
	if d.Len() < 1 || d == nil {
		return -1, errors.New("the list is empty, operation is not available")
	}
	if d.curr == nil {
		return -1, errors.New("current node not found")
	}
	id := 0
	idNode := d.head
	for d.curr != idNode {
		idNode = idNode.next
		id++
	}

	return id, nil
}

// Pop Операция Pop
func (d *DoubleLinkedList) Pop() *Node {
	d.curr = d.tail
	node := d.Current()
	_ = d.DeleteCurrent()
	node.prev = nil
	return node
}

// Shift операция shift
func (d *DoubleLinkedList) Shift() *Node {
	d.curr = d.head
	node := d.Current()
	_ = d.DeleteCurrent()
	node.next = nil
	return node
}

// SearchUUID поиск коммита по uuid
func (d *DoubleLinkedList) SearchUUID(uuID string) *Node {
	d.curr = d.head
	for i := 0; i < d.Len(); i++ {
		if d.Current().data.UUID == uuID {
			return d.Current()
		}
		d.Next()
	}
	return nil
}

// Search поиск коммита по message
func (d *DoubleLinkedList) Search(message string) *Node {
	d.curr = d.head
	for i := 0; i < d.Len(); i++ {
		if d.Current().data.Message == message {
			return d.Current()
		}
		d.Next()
	}
	return nil
}

// Reverse возвращает перевернутый список
func (d *DoubleLinkedList) Reverse() *DoubleLinkedList {
	if d == nil || d.len == 0 {
		return nil
	}
	firstCommit := *d.tail.data
	firstNode := &Node{
		data: &firstCommit,
		prev: nil,
		next: nil,
	}
	newList := &DoubleLinkedList{
		head: firstNode,
		tail: firstNode,
		curr: firstNode,
		len:  1,
	}

	d.curr = d.tail
	for i := 0; i < d.Len()-1; i++ {
		_ = newList.Insert(i, *d.Prev().data)
	}

	return newList
}

type Node struct {
	data *Commit
	prev *Node
	next *Node
}

type Commit struct {
	Message string    `json:"message"`
	UUID    string    `json:"uuid"`
	Date    time.Time `json:"date"`
}

func GenerateJSON(n int) (string, error) {
	// Дополнительное задание написать генератор данных
	// используя библиотеку gofakeit
	var dataToMarshaling []Commit
	for i := 0; i < n; i++ {
		commit, err := generateCommit()
		if err != nil {
			return "", err
		}
		dataToMarshaling = append(dataToMarshaling, *commit)
	}
	marshaledData, err := json.Marshal(dataToMarshaling)
	if err != nil {
		return "", err
	}
	path := "/home/rokzish2/go/src/gitlab.com/Rokzish/go-kata/module3/task/generatedJSON.json"
	file, err := os.Create(path)
	if err != nil {
		return "", err
	}
	_, err = file.Write(marshaledData)
	if err != nil {
		return "", err
	}

	file.Close()
	return path, nil
}

func quickSortCommits(data []Commit) []Commit {
	if len(data) <= 1 {
		return data
	}

	median := data[rand.Intn(len(data))]

	lowPart := make([]Commit, 0, len(data))
	middlePart := make([]Commit, 0, len(data))
	highPart := make([]Commit, 0, len(data))

	for _, item := range data {
		switch {
		case item.Date.Before(median.Date):
			lowPart = append(lowPart, item)
		case item.Date.After(median.Date):
			highPart = append(highPart, item)
		case item.Date.Equal(median.Date):
			middlePart = append(middlePart, item)
		}
	}
	lowPart = quickSortCommits(lowPart)
	highPart = quickSortCommits(highPart)

	lowPart = append(lowPart, middlePart...)
	lowPart = append(lowPart, highPart...)
	return lowPart
}

// generateCommit возвращает данные типа Commit, сгенерированные при помощи gofakeit
func generateCommit() (*Commit, error) {

	newCommit := &Commit{}
	err := gofakeit.Struct(newCommit)
	if err != nil {
		return nil, err
	}

	return newCommit, nil
}
