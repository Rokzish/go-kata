// https://leetcode.com/problems/subrectangle-queries/
package medium

type SubrectangleQueries struct {
	val [][]int
}

func Constructor(rectangle [][]int) SubrectangleQueries {
	return SubrectangleQueries{val: rectangle}
}

func (this *SubrectangleQueries) UpdateSubrectangle(row1 int, col1 int, row2 int, col2 int, newValue int) {

	for i := row1; i <= row2; i++ {

		for j := col1; j <= col2; j++ {
			this.val[i][j] = newValue

		}
	}
}

func (this *SubrectangleQueries) GetValue(row int, col int) int {
	return this.val[row][col]
}
