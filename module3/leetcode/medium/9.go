//https://leetcode.com/problems/arithmetic-subarrays/
package medium

import (
	"sort"
)

func checkArithmeticSubarrays(nums []int, l []int, r []int) []bool {
	res := make([]bool, len(l))

	for i := 0; i < len(l); i++ {
		tmp := nums[l[i] : r[i]+1]
		res[i] = toArithmeticArr(tmp)
	}

	return res
}

func toArithmeticArr(nums []int) bool {
	l := len(nums)
	tmp := make([]int, l)
	copy(tmp, nums)
	sort.Ints(tmp)
	if len(tmp) < 2 {
		return false
	}
	if len(tmp) == 2 {
		return true
	}
	seq := tmp[1] - tmp[0]

	for i := 2; i < l; i++ {
		if !(tmp[i]-tmp[i-1] == seq) {
			return false
		}
	}
	return true
}
