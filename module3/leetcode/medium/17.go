// https://leetcode.com/problems/queries-on-number-of-points-inside-a-circle/
package medium

import "math"

func countPoints(points [][]int, queries [][]int) []int {
	count := make([]int, len(queries))

	for i, q := range queries {

		for _, p := range points {
			if (p[0] <= q[0]+q[2] && p[0] >= q[0]-q[2]) && (p[1] <= q[1]+q[2] && p[1] >= q[1]-q[2]) {
				a := abs(p[0] - q[0])
				b := abs(p[1] - q[1])

				if math.Sqrt(math.Pow(a, 2)+math.Pow(b, 2)) <= float64(q[2]) {
					count[i]++
				}
			}

		}
	}
	return count
}
func abs(x int) float64 {
	if x < 0 {
		return float64(-x)
	}
	return float64(x)
}
