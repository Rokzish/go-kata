// https://leetcode.com/problems/group-the-people-given-the-group-size-they-belong-to/
package medium

import "fmt"

func groupThePeople(groupSizes []int) [][]int {
	groups := [][]int{}

	for p, size := range groupSizes {
		flag := false

		for i, g := range groups {
			if cap(g) == size && cap(g) > len(g) {
				groups[i] = append(groups[i], p)
				flag = true
				break
			}

		}
		if flag {
			continue
		}
		group := make([]int, 0, size)
		group = append(group, p)
		groups = append(groups, group)
		fmt.Println(groups)

	}

	return groups
}
