// https://leetcode.com/problems/binary-search-tree-to-greater-sum-tree/
package medium

func bstToGst(root *TreeNode_4) *TreeNode_4 {
	sum := 0

	var increase func(*TreeNode_4) *TreeNode_4
	increase = func(node *TreeNode_4) *TreeNode_4 {
		if node == nil {
			return nil
		}
		newNode := &TreeNode_4{}
		newNode.Right = increase(node.Right)
		sum += node.Val
		newNode.Val = sum
		newNode.Left = increase(node.Left)
		return newNode
	}

	return increase(root)
}

type TreeNode_4 struct {
	Val   int
	Left  *TreeNode_4
	Right *TreeNode_4
}
