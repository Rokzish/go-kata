//https://leetcode.com/problems/letter-tile-possibilities/
package medium

func numTilePossibilities(tiles string) int {
	all := make(map[string]struct{})

	var chars []string
	for i := 0; i < len(tiles); i++ {
		char := string(tiles[i])
		n := len(chars)
		all[char] = struct{}{}
		chars = append(chars, char)

		for j := 0; j < n; j++ {
			for k := 0; k <= len(chars[j]); k++ {
				item := chars[j]
				item = item[:k] + char + item[k:]
				all[item] = struct{}{}
				chars = append(chars, item)
			}
		}
	}
	return len(all)
}
