// https://leetcode.com/problems/partitioning-into-minimum-number-of-deci-binary-numbers/
package medium

func minPartitions(n string) int {
	max := 0
	for _, v := range n {
		tmp := int(v) - 48
		if tmp > max {
			max = tmp
		}
	}

	return max
}
