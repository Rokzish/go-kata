// https://leetcode.com/problems/maximum-binary-tree/
package medium

type TreeNode_6 struct {
	Val   int
	Left  *TreeNode_6
	Right *TreeNode_6
}

func constructMaximumBinaryTree(nums []int) *TreeNode_6 {
	if len(nums) < 1 {
		return nil
	}
	tree := &TreeNode_6{
		Val:   0,
		Left:  nil,
		Right: nil,
	}
	id := 0
	for i, v := range nums {
		if tree.Val < v {
			tree.Val = v
			id = i
		}
	}
	tree.Right = constructMaximumBinaryTree(nums[id+1:])

	tree.Left = constructMaximumBinaryTree(nums[:id])
	return tree

}
