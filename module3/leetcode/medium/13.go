// https://leetcode.com/problems/maximum-sum-of-an-hourglass/
package medium

func maxSum(grid [][]int) int {
	sum := 0

	//sum = grid[0][0] + grid[0][1] + grid[0][2] + grid[1][1] + grid[2][0] + grid[2][1] + grid[2][2]

	for i := 0; i+2 < len(grid); i++ {
		for j := 0; j+2 < len(grid[i]); j++ {
			tmp := grid[i][j] + grid[i][j+1] + grid[i][j+2] + grid[i+1][j+1] + grid[i+2][j] + grid[i+2][j+1] + grid[i+2][j+2]
			if tmp > sum {
				sum = tmp
			}
		}

	}

	return sum
}
