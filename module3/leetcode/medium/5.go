// https://leetcode.com/problems/maximum-twin-sum-of-a-linked-list/
package medium

type ListNode_5 struct {
	Val  int
	Next *ListNode_5
}

func pairSum(head *ListNode_5) int {
	vals := []int{}
	for n := head; n != nil; n = n.Next {
		vals = append(vals, n.Val)
	}
	l := len(vals)
	max := 0
	for i := 0; i < l/2; i++ {
		sum := vals[i] + vals[l-1-i]
		if max < sum {
			max = sum
		}
	}
	return max
}
