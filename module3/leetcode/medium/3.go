// https://leetcode.com/problems/merge-nodes-in-between-zeros/
package medium

func mergeNodes(head *ListNode) *ListNode {
	newList := &ListNode{
		Val:  0,
		Next: nil,
	}
	res := newList

	for n := head; n.Next != nil; n = n.Next {
		if n.Val == 0 {
			newList.Next = &ListNode{}
			newList = newList.Next
		} else {
			newList.Val += n.Val
		}

	}

	return res.Next
}

type ListNode struct {
	Val  int
	Next *ListNode
}
