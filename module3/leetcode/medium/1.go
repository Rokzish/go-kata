//https://leetcode.com/problems/deepest-leaves-sum/
package medium

func deepestLeavesSum(root *TreeNode) int {
	sum := 0
	layer := 0

	sum, _ = getLeaves(root, layer)
	return sum
}
func getLeaves(node *TreeNode, layer int) (int, int) {
	if node == nil {
		return 0, 0
	}
	layer++

	if node.Left == nil && node.Right == nil {
		return node.Val, layer
	}
	leftVal, leftLayer := 0, 0
	rightVal, rightLayer := 0, 0

	if node.Left != nil {
		leftVal, leftLayer = getLeaves(node.Left, layer)
	}

	if node.Right != nil {
		rightVal, rightLayer = getLeaves(node.Right, layer)
	}

	if leftLayer > rightLayer {
		return leftVal, leftLayer
	} else if leftLayer < rightLayer {
		return rightVal, rightLayer
	} else {
		return rightVal + leftVal, leftLayer
	}

}

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}
