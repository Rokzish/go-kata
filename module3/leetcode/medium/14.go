// https://leetcode.com/problems/xor-queries-of-a-subarray/
package medium

func xorQueries(arr []int, queries [][]int) []int {
	res := make([]int, len(queries))
	for i := 0; i < len(queries); i++ {

		tmp := 0
		for j := queries[i][0]; j <= queries[i][1]; j++ {

			tmp ^= arr[j]
		}
		res[i] = tmp
	}
	return res
}
