// https://leetcode.com/problems/kth-missing-positive-number/description/
package easy

func findKthPositive(arr []int, k int) int {
	res := 0
	for i := 0; i < k; {
		res++
		x := false
		for _, v := range arr {
			if res == v {
				x = true
				break
			}
		}
		if !x {
			i++
		}
	}

	return res
}
