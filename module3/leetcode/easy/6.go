//https://leetcode.com/problems/unique-morse-code-words/
package easy

import "fmt"

func uniqueMorseRepresentations(words []string) int {
	morseCode := []string{".-", "-...", "-.-.", "-..", ".", "..-.", "--.", "....", "..", ".---", "-.-", ".-..", "--", "-.", "---", ".--.", "--.-", ".-.", "...", "-", "..-", "...-", ".--", "-..-", "-.--", "--.."}
	morseWords := make([]string, 0, len(words))
	for _, enWord := range words {
		var morseWord string
		for _, l := range enWord {
			morseWord += morseCode[l-97]
		}
		morseWords = append(morseWords, morseWord)
	}

	for _, v := range morseWords {
		fmt.Println(v)
	}
	extra := make([]string, 0)

	for _, word := range morseWords {
		flag := false
		for _, exWord := range extra {
			if word == exWord {
				flag = true
			}
		}
		if !flag {
			extra = append(extra, word)
		}
	}
	return len(extra)
}
