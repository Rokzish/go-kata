// https://leetcode.com/problems/kids-with-the-greatest-number-of-candies/
package easy

func kidsWithCandies(candies []int, extraCandies int) []bool {
	result := make([]bool, len(candies))
	max := 0
	for _, v := range candies {
		if v > max {
			max = v
		}
	}
	for i, v := range candies {
		if v+extraCandies >= max {
			result[i] = true
		}
	}
	return result
}
