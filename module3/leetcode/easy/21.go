// https://leetcode.com/problems/subtract-the-product-and-sum-of-digits-of-an-integer/
package easy

func subtractProductAndSum(n int) int {
	var sum, mult int = 0, 1

	for n > 0 {
		x := n % 10
		sum += x
		mult *= x
		n /= 10
	}
	res := mult - sum

	return res

}
