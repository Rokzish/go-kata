// https://leetcode.com/problems/split-a-string-in-balanced-strings/
package easy

func balancedStringSplit(s string) int {
	count := 0
	r := 0
	l := 0
	for _, v := range s {
		if v == 'R' {
			r++
		} else {
			l++
		}
		if r == l && r != 0 {
			count++
			r = 0
			l = 0
		}
	}
	return count
}
