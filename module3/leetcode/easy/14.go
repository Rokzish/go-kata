// https://leetcode.com/problems/richest-customer-wealth/
package easy

func maximumWealth(accounts [][]int) int {
	maxSum := 0
	for _, banks := range accounts {
		sum := 0
		for _, v := range banks {
			sum += v
		}
		if maxSum < sum {
			maxSum = sum
		}
	}
	return maxSum
}
