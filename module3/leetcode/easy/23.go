// https://leetcode.com/problems/goal-parser-interpretation/
package easy

func interpret(command string) string {
	var res string
	for i := 0; i < len(command); i++ {
		if command[i] == 'G' {
			res += "G"
		} else if command[i] == '(' {
			if command[i+1] == ')' {
				res += "o"
			} else {
				res += "al"
			}
		}
	}
	return res
}
