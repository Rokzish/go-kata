// https://leetcode.com/problems/running-sum-of-1d-array/
package easy

func runningSum(nums []int) []int {
	sum := make([]int, len(nums))
	for i := 0; i < len(nums); i++ {
		if i == 0 {
			sum[i] = nums[i]
		} else {
			sum[i] = sum[i-1] + nums[i]
		}
	}
	return sum
}
