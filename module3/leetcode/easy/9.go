// https://leetcode.com/problems/final-value-of-variable-after-performing-operations/
package easy

func finalValueAfterOperations(operations []string) int {
	dict := map[string]int{"++X": 1, "X++": 1, "--X": -1, "X--": -1}
	x := 0
	for _, v := range operations {
		x += dict[v]
	}
	return x
}
