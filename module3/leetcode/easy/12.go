// https://leetcode.com/problems/number-of-good-pairs/
package easy

func numIdenticalPairs(nums []int) int {
	count := 0
	for i := 0; i < len(nums)-1; i++ {
		for j := i + 1; j < len(nums); j++ {
			if nums[i] == nums[j] {
				count++
			}
		}
	}
	return count
}
