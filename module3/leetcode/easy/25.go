// https://leetcode.com/problems/create-target-array-in-the-given-order/
package easy

func createTargetArray(nums []int, index []int) []int {
	tgArr := make([]int, 0, len(nums))
	for i := 0; i < len(nums); i++ {
		tgArr = append(tgArr[:index[i]], append([]int{nums[i]}, tgArr[index[i]:]...)...)
	}
	return tgArr
}
