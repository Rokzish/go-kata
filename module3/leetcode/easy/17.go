// https://leetcode.com/problems/maximum-number-of-words-found-in-sentences/
package easy

func mostWordsFound(sentences []string) int {
	max := 0
	for _, sentence := range sentences {
		length := 0
		if len(sentence) > 0 {
			length++
		}
		for _, v := range sentence {
			if v == ' ' {
				length++
			}
		}
		if length > max {
			max = length
		}
	}
	return max
}
