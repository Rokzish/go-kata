// https://leetcode.com/problems/how-many-numbers-are-smaller-than-the-current-number/
package easy

func smallerNumbersThanCurrent(nums []int) []int {
	l := len(nums)
	res := make([]int, l)
	for id, v := range nums {
		for i := 0; i < l; i++ {
			if v > nums[i] {
				res[id]++
			}
		}
	}
	return res
}
