// https://leetcode.com/problems/count-the-digits-that-divide-a-number/
package easy

func countDigits(num int) int {
	nums := []int{}
	tmp := num
	for tmp > 0 {
		nums = append(nums, tmp%10)
		tmp /= 10
	}
	res := 0
	for _, v := range nums {
		if num%v == 0 {
			res++
		}
	}
	return res
}
