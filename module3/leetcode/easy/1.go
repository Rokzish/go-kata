//https://leetcode.com/problems/n-th-tribonacci-number/description/
package easy

func tribonacci(n int) int {
	switch n {
	case 0:
		return 0
	case 1:
		return 1
	case 2:
		return 1
	default:
		var tn0, tn1, tn2 int = 0, 1, 1
		var tn int
		for i := 3; i <= n; i++ {
			tn = tn0 + tn1 + tn2
			tn0, tn1, tn2 = tn1, tn2, tn
		}
		return tn
	}
}
