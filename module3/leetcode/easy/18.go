// https://leetcode.com/problems/difference-between-element-sum-and-digit-sum-of-an-array/
package easy

func differenceOfSum(nums []int) int {
	var sumElem, sumDigits int
	for _, v := range nums {
		sumElem += v
		for v > 0 {
			sumDigits += v % 10
			v /= 10
		}
	}
	return abs(sumElem - sumDigits)
}

func abs(n int) int {
	if n < 0 {
		return n * -1
	}
	return n
}
