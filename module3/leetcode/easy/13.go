// https://leetcode.com/problems/jewels-and-stones/
package easy

func numJewelsInStones(jewels string, stones string) int {

	count := 0
	jewelMap := make(map[rune]struct{})
	for _, v := range jewels {
		_, ok := jewelMap[v]
		if !ok {
			jewelMap[v] = struct{}{}
		}
	}

	for _, v := range stones {
		_, ok := jewelMap[v]
		if ok {
			count++
		}
	}
	return count
}
