// https://leetcode.com/problems/count-good-triplets/
package easy

func countGoodTriplets(arr []int, a int, b int, c int) int {
	count := 0
	var dif1, dif2, dif3 int
	for i := 0; i < len(arr)-2; i++ {
		for j := i + 1; j < len(arr)-1; j++ {
			for k := j + 1; k < len(arr); k++ {
				dif1 = abS(arr[i] - arr[j])
				dif2 = abS(arr[j] - arr[k])
				dif3 = abS(arr[i] - arr[k])
				if dif1 <= a && dif2 <= b && dif3 <= c {
					count++
				}
			}
		}
	}
	return count
}

func abS(n int) int {
	if n < 0 {
		return n * -1
	}
	return n
}
