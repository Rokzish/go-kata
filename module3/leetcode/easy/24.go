// https://leetcode.com/problems/decode-xored-array/
package easy

func decode(encoded []int, first int) []int {
	arr := []int{first}

	for _, v := range encoded {
		arr = append(arr, arr[len(arr)-1]^v)
	}
	return arr
}
