// https://leetcode.com/problems/sort-the-people/
package easy

func sortPeople(names []string, heights []int) []string {
	swapped := true
	i := 1
	for swapped {
		swapped = false
		for j := 0; j < len(names)-i; j++ {
			if heights[j] < heights[j+1] {
				heights[j], heights[j+1] = heights[j+1], heights[j]
				names[j], names[j+1] = names[j+1], names[j]
				swapped = true
			}
		}
		i++
	}

	return names
}
