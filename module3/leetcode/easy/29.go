// https://leetcode.com/problems/xor-operation-in-an-array/
package easy

func xorOperation(n int, start int) int {

	res := start

	for i := 1; i < n; i++ {
		res ^= start + i*2
	}
	return res
}
