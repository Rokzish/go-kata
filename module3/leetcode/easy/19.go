// https://leetcode.com/problems/minimum-sum-of-four-digit-number-after-splitting-digits/
package easy

import "sort"

func minimumSum(num int) int {
	n := make([]int, 0, 4)

	for num > 0 {
		n = append(n, num%10)
		num /= 10
	}
	sort.Ints(n)

	new1 := n[0]*10 + n[2]
	new2 := n[1]*10 + n[3]
	return new1 + new2
}
