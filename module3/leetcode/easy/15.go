// https://leetcode.com/problems/design-parking-system/
package easy

type ParkingSystem struct {
	big        int
	medium     int
	small      int
	currBig    int
	currMedium int
	currSmall  int
}

func (p *ParkingSystem) AddCar(carType int) bool {
	var check bool
	switch carType {
	case 1:
		if p.big == p.currBig {
			check = false
		} else {
			p.currBig++
			check = true
		}
	case 2:
		if p.medium == p.currMedium {
			check = false
		} else {
			p.currMedium++
			check = true
		}
	case 3:
		if p.small == p.currSmall {
			check = false
		} else {
			p.currSmall++
			check = true
		}
	}
	return check
}

func Constructor(big, medium, small int) ParkingSystem {
	return ParkingSystem{
		big:    big,
		medium: medium,
		small:  small,
	}
}
