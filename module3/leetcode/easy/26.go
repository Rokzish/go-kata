// https://leetcode.com/problems/decompress-run-length-encoded-list/
package easy

func decompressRLElist(nums []int) []int {
	res := []int{}
	for i := 0; i < len(nums); i += 2 {
		new := []int{}
		for j := 0; j < nums[i]; j++ {
			new = append(new, nums[i+1])
		}
		res = append(res, new...)
	}
	return res
}
