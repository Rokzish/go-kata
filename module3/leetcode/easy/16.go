// https://leetcode.com/problems/smallest-even-multiple/
package easy

func smallestEvenMultiple(n int) int {
	res := 1
	for ; ; res++ {
		if res%n == 0 && res%2 == 0 {
			break
		}

	}
	return res
}
