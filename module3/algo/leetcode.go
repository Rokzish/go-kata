package algo

func minimumAverageDifference(nums []int) int {
	minDiff := 922337203684775807
	var sumRight int
	for i := range nums {
		sumRight += nums[i]
	}

	var idx int
	var sumLeft int
	var avgDiff int
	rightCount := len(nums)
	for i := 0; i < len(nums); i++ {
		sumLeft += nums[i]
		sumRight -= nums[i]
		rightCount--
		if rightCount == 0 {
			rightCount++
		}
		avgDiff = sumLeft/(i+1) - sumRight/rightCount
		if avgDiff < 0 {
			avgDiff *= -1
		}
		if avgDiff < minDiff {
			minDiff = avgDiff
			idx = i
		}
	}
	return idx
}
