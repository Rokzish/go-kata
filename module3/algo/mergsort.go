package algo

func mergeSort(item []int) []int {
	if len(item) < 2 {
		return item
	}

	first := mergeSort(item[:len(item)/2])
	second := mergeSort(item[len(item)/2:])

	return merge(first, second)
}

func merge(a, b []int) []int {
	final := []int{}
	i := 0
	j := 0
	for i < len(a) && j < len(b) {
		if a[i] < b[j] {
			final = append(final, a[i])
			i++
		} else {
			final = append(final, b[j])
			j++
		}
	}
	for ; i < len(a); i++ {
		final = append(final, a[i])
	}
	for ; j < len(b); j++ {
		final = append(final, b[j])
	}

	return final
}
