package main

import (
	"fmt"
	"time"
)

type Post struct {
	body        string
	publishDate int64 // Unix timestamp
	next        *Post
}

type Feed struct {
	length int // we'll use it later
	start  *Post
	end    *Post
}

// Реализуйте метод Append для добавления нового поста в конец потока
func (f *Feed) Append(newPost *Post) {
	if f.start == nil {
		f.start = newPost
		f.end = newPost
	} else {
		f.end.next = newPost
		f.end = newPost

	}
	f.length++
}

// Реализуйте метод Remove для удаления поста по дате публикации из потока
func (f *Feed) Remove(publishDate int64) {
	// ваш код здесь
	if f.length == 0 {
		panic("Empty Feed")
	}
	var previousPost *Post
	currentPost := f.start

	for currentPost.publishDate != publishDate {
		if currentPost.next == nil {
			panic("Not found Post")
		}
		previousPost = currentPost
		currentPost = currentPost.next
	}
	previousPost.next = currentPost.next

	f.length--

}

// Реализуйте метод Insert для вставки нового поста в поток в соответствии с его датой публикации
func (f *Feed) Insert(newPost *Post) {
	// ваш код здесь
	if f.length == 0 {
		f.start, f.end = newPost, newPost
	} else {
		var previousPost *Post
		currentPost := f.start
		for currentPost.publishDate < newPost.publishDate {
			previousPost = currentPost
			currentPost = currentPost.next
		}
		previousPost.next = newPost
		newPost.next = currentPost
	}
	f.length++
}

// Реализуйте метод Inspect для вывода информации о потоке и его постах
func (f *Feed) Inspect() {
	// ваш код здесь
	if f.length == 0 {
		fmt.Println("Empty Feed")
	}
	currentIndex := 0
	currentPost := f.start
	fmt.Println("==============================")
	fmt.Printf("Feed lenght: %v\n", f.length)
	for currentIndex < f.length {
		fmt.Printf("Item: %d - %v\n", currentIndex, currentPost)
		currentPost = currentPost.next
		currentIndex++
	}
	fmt.Println("==============================")
}

func main() {
	rightNow := time.Now().Unix()
	f := &Feed{}
	p1 := &Post{
		body:        "Lorem ipsum",
		publishDate: rightNow,
	}
	p2 := &Post{
		body:        "Dolor sit amet",
		publishDate: rightNow + 10,
	}
	p3 := &Post{
		body:        "consectetur adipiscing elit",
		publishDate: rightNow + 20,
	}
	p4 := &Post{
		body:        "sed do eiusmod tempor incididunt",
		publishDate: rightNow + 30,
	}
	f.Append(p1)
	f.Append(p2)
	f.Append(p3)
	f.Append(p4)

	newPost := &Post{
		body:        "This is a new post",
		publishDate: rightNow + 15,
	}
	f.Insert(newPost)
	f.Inspect()

	f.Remove(p3.publishDate)

	f.Inspect()
}

// p3 в main не убирал, только добавлял новые вызовы
