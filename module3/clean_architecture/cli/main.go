package main

import (
	"fmt"
	"os"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/repo"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/model"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/service"
)

const filePath = "/home/rokzish2/go/src/gitlab.com/Rokzish/go-kata/module3/clean_architecture/cli/cli.json"

func init() {
	file, err := os.OpenFile(filePath, os.O_WRONLY, 0755)
	if err != nil {
		panic(err)
	}
	err = file.Truncate(0)
	if err != nil {
		panic(err)
	}

}

func main() {

	var taskRepo service.TodoService
	taskRepo.Repository = repo.TaskRepository(&repo.FileTaskRepository{FilePath: filePath})
	exit := false
	for !exit {
		num := 0
		fmt.Println("Select an action:\n   1. Show task list.\n   2. Add a task.\n   3. Delete task.\n   4. Edit task by ID.\n   5. Complete the task.\n   6. Exit.")
		fmt.Print("Enter number:")
		fmt.Scan(&num)

		switch num {
		case 1: // show task
			tasks, err := taskRepo.ListTodos()
			if err != nil {
				panic(err)
			}
			if len(tasks) < 1 {
				fmt.Println("-Task list is empty")
				continue
			}
			fmt.Println("-Task list:")
			for _, v := range tasks {
				fmt.Println("   ID:", v.ID, " Title:", v.Title, "  Description:", v.Description, "  Status:", v.Status)
			}
		case 2: //add task
			var task model.Todo
			fmt.Print("-Enter the title of the new task:")
			fmt.Scanln(&task.Title)
			fmt.Print("-Enter  description of the new task:")
			fmt.Scanln(&task.Description)
			fmt.Print("-Enter the status of the new task:")
			fmt.Scanln(&task.Status)
			err := taskRepo.CreateTodo(task)
			if err != nil {
				panic(err)
			}
			fmt.Println("-New task added.")

		case 3: //delete task
			tasks, err := taskRepo.ListTodos()
			if err != nil {
				panic(err)
			}
			if len(tasks) < 1 {
				fmt.Println("-Task list is empty")
				continue
			}
			fmt.Println("-Task list:")
			for _, v := range tasks {
				fmt.Println("   ID:", v.ID, " Title:", v.Title, "  Description:", v.Description, "  Status:", v.Status)
			}
			id := 0
			fmt.Print("-Enter task ID to delete:")
			fmt.Scanln(&id)
			if id > 0 && id <= len(tasks) {
				_ = taskRepo.RemoveTodo(tasks[id-1])
			} else {
				fmt.Println("-ID out of range.")
				continue
			}
			fmt.Println("-Chosen task deleted.")

		case 4: // edit task
			tasks, err := taskRepo.ListTodos()
			if err != nil {
				panic(err)
			}
			if len(tasks) < 1 {
				fmt.Println("-Task list is empty")
				continue
			}
			fmt.Println("-Task list:")
			for _, v := range tasks {
				fmt.Println("   ID:", v.ID, " Title:", v.Title, "  Description:", v.Description, "  Status:", v.Status)
			}
			id := 0
			fmt.Print("-Enter task ID to update:")
			fmt.Scanln(&id)
			if id < 1 || id > len(tasks) {
				fmt.Println("-ID out of range.")
				continue
			}

			updatedTask := tasks[id-1]
			end := false
			for !end {
				var x int
				fmt.Println("-Select what to update:")
				fmt.Println("   1. Title\n   2. Description.\n   3. Status.\n   4. End update.")
				fmt.Print("-Enter number:")
				fmt.Scanln(&x)

				switch x {
				case 1: // title
					fmt.Print("-Enter new title:")
					fmt.Scanln(&updatedTask.Title)
				case 2: // description
					fmt.Print("-Enter new description:")
					fmt.Scanln(&updatedTask.Description)
				case 3: // status
					fmt.Print("-Enter new status:")
					fmt.Scanln(&updatedTask.Status)
				case 4: // end update
					end = true
				default:
					fmt.Println("-Action not defined, try again.")
				}
			}
			err = taskRepo.UpdateTodo(updatedTask)
			if err != nil {
				panic(err)
			}
			fmt.Println("-Chosen task updated.")
		case 5: // complete task
			tasks, err := taskRepo.ListTodos()
			if err != nil {
				panic(err)
			}
			if len(tasks) < 1 {
				fmt.Println("-Task list is empty")
				continue
			}
			fmt.Println("-Task list:")
			for _, v := range tasks {
				fmt.Println("   ID:", v.ID, " Title:", v.Title, "  Description:", v.Description, "  Status:", v.Status)
			}
			id := 0
			fmt.Print("-Enter task ID to complete:")
			fmt.Scanln(&id)
			if id > 0 && id <= len(tasks) {
				if tasks[id-1].Status == "Done" {
					fmt.Println("-Task already completed")
					continue
				}
				_ = taskRepo.CompleteTodo(tasks[id-1])
			} else {
				fmt.Println("-ID out of range.")
				continue
			}
			fmt.Println("-Chosen task completed.")
		case 6: // exit
			exit = true
		default:
			fmt.Println("-Action not defined, try again.")
		}
	}
	fmt.Println("Program shutdown")
}
