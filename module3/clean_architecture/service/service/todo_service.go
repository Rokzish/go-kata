package service

import (
	"errors"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/model"
)

type TodoServicer interface {
	ListTodos() ([]model.Todo, error)
	CreateTodo(todo model.Todo) error
	CompleteTodo(todo model.Todo) error
	RemoveTodo(todo model.Todo) error
	UpdateTodo(todo model.Todo) error
}

type TodoRepository interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(task model.Todo) error
	UpdateTask(task model.Todo) error
	DeleteTask(id int) error
}

type TodoService struct {
	Repository TodoRepository
}

func (s *TodoService) ListTodos() ([]model.Todo, error) {
	todos, err := s.Repository.GetTasks()
	if err != nil {
		return nil, err
	}

	return todos, nil
}

func (s *TodoService) CreateTodo(todo model.Todo) error {

	err := s.Repository.CreateTask(todo)
	if err != nil {
		return err
	}

	return nil
}

func (s *TodoService) CompleteTodo(todo model.Todo) error {
	todos, err := s.ListTodos()
	if err != nil {
		return err
	}
	check := false
	for _, v := range todos {
		if v == todo {
			check = true
			break
		}
	}
	if !check {
		return errors.New("todo to complete not found")
	}

	// some Job
	todo.Status = "Done"

	if err := s.Repository.UpdateTask(todo); err != nil {
		return err
	}
	return nil
}

func (s *TodoService) RemoveTodo(todo model.Todo) error {
	todos, err := s.ListTodos()
	if err != nil {
		return err
	}
	check := false
	for _, v := range todos {
		if v == todo {
			check = true
			break
		}
	}
	if !check {
		return errors.New("todo to remove not found")
	}
	if err := s.Repository.DeleteTask(todo.ID); err != nil {
		return err
	}
	return nil

}

func (s *TodoService) UpdateTodo(todo model.Todo) error {

	err := s.Repository.UpdateTask(todo)
	if err != nil {
		return err
	}
	return nil
}
