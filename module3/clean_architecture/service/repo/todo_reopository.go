package repo

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"os"

	"gitlab.com/Rokzish/go-kata/module3/clean_architecture/service/model"
)

type TaskRepository interface {
	GetTasks() ([]model.Todo, error)
	GetTask(id int) (model.Todo, error)
	CreateTask(task model.Todo) error
	UpdateTask(task model.Todo) error
	DeleteTask(id int) error
}

type FileTaskRepository struct {
	FilePath string
}

func NewFileTaskRepository(filePath string) *FileTaskRepository {
	return &FileTaskRepository{FilePath: filePath}
}

func (repo *FileTaskRepository) GetTasks() ([]model.Todo, error) {
	tasks := []model.Todo{}

	file, err := os.OpenFile(repo.FilePath, os.O_RDONLY, 0755)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	if len(content) > 0 {
		if err := json.Unmarshal(content, &tasks); err != nil {
			return nil, err
		}
	}
	return tasks, nil
}

func (repo *FileTaskRepository) GetTask(id int) (model.Todo, error) {
	var task model.Todo

	tasks, err := repo.GetTasks()
	if err != nil {
		return task, err
	}
	if id < 1 || id > len(tasks) {
		return model.Todo{}, errors.New("ID out of range")
	}

	for _, t := range tasks {
		if t.ID == id {
			task = t
			break
		}
	}

	return task, nil
}

func (repo *FileTaskRepository) CreateTask(task model.Todo) error {
	var tasks []model.Todo
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}

	tasks = append(tasks, task)

	if err := repo.saveTasks(tasks); err != nil {
		return err
	}

	return nil
}

func (repo *FileTaskRepository) saveTasks(tasks []model.Todo) error {
	file, err := os.OpenFile(repo.FilePath, os.O_WRONLY, 0755)
	if err != nil {
		return err
	}
	defer file.Close()
	for i := 0; i < len(tasks); i++ {
		tasks[i].ID = i + 1
	}

	toSave, err := json.MarshalIndent(tasks, "", " ")
	if err != nil {
		return err
	}

	err = file.Truncate(0)
	if err != nil {
		return err
	}
	if _, err = file.Write(toSave); err != nil {
		return err
	}

	return nil

}

func (repo *FileTaskRepository) UpdateTask(task model.Todo) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}
	check := false
	for i, t := range tasks {
		if task.ID == t.ID {
			tasks[i] = task
			check = true
			break
		}
	}
	if !check {
		return errors.New("task to update not found")
	}

	if err := repo.saveTasks(tasks); err != nil {
		return err
	}
	return nil

}

func (repo *FileTaskRepository) DeleteTask(id int) error {
	tasks, err := repo.GetTasks()
	if err != nil {
		return err
	}

	if id < 1 || id > len(tasks) {
		return errors.New("task to delete not found")
	}

	tasks = append(tasks[:id-1], tasks[id:]...)
	if err := repo.saveTasks(tasks); err != nil {
		return err
	}
	return nil

}
