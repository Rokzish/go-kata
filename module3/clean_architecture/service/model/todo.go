package model

type Todo struct {
	ID          int
	Title       string
	Description string
	Status      string
}
