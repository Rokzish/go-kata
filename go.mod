module gitlab.com/Rokzish/go-kata

go 1.18

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/alexsergivan/transliterator v1.0.0
	github.com/brianvoe/gofakeit/v6 v6.20.1
	github.com/go-chi/chi/v5 v5.0.8
	github.com/go-chi/render v1.0.2
	github.com/json-iterator/go v1.1.12
	github.com/mattn/go-sqlite3 v1.14.16
	github.com/stretchr/testify v1.8.1
	github.com/tebeka/selenium v0.9.9
	gitlab.com/Rokzish/greet v0.0.1
	golang.org/x/sync v0.1.0
)

require (
	github.com/ajg/form v1.5.1 // indirect
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/kr/pretty v0.3.1 // indirect
	github.com/modern-go/concurrent v0.0.0-20180228061459-e0a39a4cb421 // indirect
	github.com/modern-go/reflect2 v1.0.2 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	golang.org/x/net v0.7.0 // indirect
	gopkg.in/check.v1 v1.0.0-20201130134442-10cb98267c6c // indirect
	gopkg.in/yaml.v3 v3.0.1 // indirect
)
