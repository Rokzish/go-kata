package benching

import (
	"fmt"
	"testing"
)

func insertXIntSlice(x int, b *testing.B) {
	testSlice := make([]int, 0)
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testSlice = append(testSlice, i)
		fmt.Println(testSlice)
	}

}

// BenchmarkInsertIntSlice1000000 тестирует скорость вставки 1000000 целых чисел в срез.
func BenchmarkInsertIntSlice1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(1000000, b)
	}
}

// BenchmarkInsertIntSlice100000 тестирует скорость вставки 100000 целых чисел в срез.
func BenchmarkInsertIntSlice100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(100000, b)
	}
}

// BenchmarkInsertIntSlice10000 тестирует скорость вставки 10000 целых чисел в срез.
func BenchmarkInsertIntSlice10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(10000, b)
	}
}

// BenchmarkInsertIntSlice1000 тестирует скорость вставки 1000 целых чисел в срез.
func BenchmarkInsertIntSlice1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(1000, b)
	}
}

// BenchmarkInsertIntSlice100 тестирует скорость вставки 100 целых чисел в срез.
func BenchmarkInsertIntSlice100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXIntSlice(100, b)
	}
}
