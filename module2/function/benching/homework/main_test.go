package main

import "testing"

func BenchmarkMapUserProducts1(b *testing.B) {
	products := genProducts()
	users := genUsers()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = MapUserProducts(users, products)

	}
}

func BenchmarkMapUserProducts2(b *testing.B) {
	products := genProducts()
	users := genUsers()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		_ = MapUserProducts2(users, products)

	}
}
