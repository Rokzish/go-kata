package benching

import (
	"fmt"
	"testing"
)

// insertXPreallocIntMap - для добавления X элементов в Map[int]int
func insertXPreallocIntMap(x int, b *testing.B) {
	// Инициализируем Map, вставляем X элементов и заранее предустанавливаем размер X
	testmap := make(map[int]int, x)
	// Сбрасываем таймер после инициализации Map
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Вставляем значение I в ключ I.
		testmap[i] = i
	}
}

// BenchmarkInsertIntMapPreAlloc1000000 тестирует скорость вставки 1000000 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(1000000, b)
	}
}

// BenchmarkInsertIntMapPreAlloc100000 тестирует скорость вставки 100000 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(100000, b)
	}
}

// BenchmarkInsertIntMapPreAlloc10000 тестирует скорость вставки 10000 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(10000, b)
	}
}

// BenchmarkInsertIntMapPreAlloc1000 тестирует скорость вставки 1000 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(1000, b)
	}
}

// BenchmarkInsertIntMapPreAlloc100 тестирует скорость вставки 100 целых чисел в карту.
func BenchmarkInsertIntMapPreAlloc100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntMap(100, b)
	}
}

// selectXIntMap применяется для выборки X элементов из карты
func selectXIntMap(x int, b *testing.B) {
	// Инициализация Map и вставка X элементов
	testmap := make(map[int]int, x)
	// Сброс таймера после инициализации Map
	for i := 0; i < x; i++ {
		// Вставка значения I в ключ I.
		testmap[i] = i
	}
	// holder нужен для хранения найденного int, нельзя извлекать из карты значение без сохранения результата
	var holder int
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Выборка из карты
		holder = testmap[i]
	}
	// Компилятор не оставит без внимания неиспользованный holder, но с помощью быстрой проверки мы его обхитрим.
	if holder != 0 {
		fmt.Println(holder)
	}
}

// BenchmarkSelectIntMap1000000 тестирует скорость выборки 1000000 элементов из карты.
func BenchmarkSelectIntMap1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(1000000, b)
	}
}

// BenchmarkSelectIntMap100000 тестирует скорость выборки 100000 элементов из карты.
func BenchmarkSelectIntMap100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(100000, b)
	}
}

// BenchmarkSelectIntMap10000 тестирует скорость выборки 10000 элементов из карты.
func BenchmarkSelectIntMap10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(10000, b)
	}
}

// BenchmarkSelectIntMap1000 тестирует скорость выборки 1000 элементов из карты.
func BenchmarkSelectIntMap1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(1000, b)
	}
}

// BenchmarkSelectIntMap100 тестирует скорость выборки 100 элементов из карты.
func BenchmarkSelectIntMap100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntMap(100, b)
	}
}
