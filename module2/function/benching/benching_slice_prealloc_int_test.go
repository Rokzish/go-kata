package benching

import (
	"fmt"
	"testing"
)

// insertXPreallocIntSlice - для добавления X элементов в []int
func insertXPreallocIntSlice(x int, b *testing.B) {
	// Инициализация Slice и вставка X элементов
	testSlice := make([]int, x)
	// сброс таймера
	b.ResetTimer()
	for i := 0; i < x; i++ {
		testSlice[i] = i
	}
}

// BenchmarkInsertIntSlicePreAllooc1000000 тестирует скорость вставки 1000000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAlloc1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(1000000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc100000 тестирует скорость вставки 100000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAlloc100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(100000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc10000 тестирует скорость вставки 10000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAlloc10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(10000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc1000 тестирует скорость вставки 1000 целых чисел в срез.
func BenchmarkInsertIntSlicePreAlloc1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(1000, b)
	}
}

// BenchmarkInsertIntSlicePreAllooc100 тестирует скорость вставки 100 целых чисел в срез.
func BenchmarkInsertIntSlicePreAlloc100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		insertXPreallocIntSlice(100, b)
	}
}

// selectXIntSlice - для выборки X элементов из среза
func selectXIntSlice(x int, b *testing.B) {
	// Инициализация Slice и вставка X элементов
	testSlice := make([]int, x)
	// Сброс таймера после инициализации Slice
	for i := 0; i < x; i++ {
		// Вставка значения I в ключ I.
		testSlice[i] = i
	}
	// holder нужен для хранения найденного int, так как нельзя извлекать значение из среза без сохранения результата
	var holder int
	b.ResetTimer()
	for i := 0; i < x; i++ {
		// Select from Slice
		holder = testSlice[i]
	}
	// Компилятор не оставит без внимания неиспользованный holder, но с помощью быстрой проверки мы его обхитрим.
	if holder != 0 {
		fmt.Println(holder)
	}
}

// BenchmarkSelectIntSlice1000000 тестирует скорость выборки 1000000 элементов из среза.
func BenchmarkSelectIntSlice1000000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntSlice(1000000, b)
	}
}

// BenchmarkSelectIntSlice100000 тестирует скорость выборки 100000 элементов из среза.
func BenchmarkSelectIntSlice100000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntSlice(100000, b)
	}
}

// BenchmarkSelectIntSlice10000 тестирует скорость выборки 10000 элементов из среза.
func BenchmarkSelectIntSlice10000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntSlice(10000, b)
	}
}

// BenchmarkSelectIntSlice1000 тестирует скорость выборки 1000 элементов из среза.
func BenchmarkSelectIntSlice1000(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntSlice(1000, b)
	}
}

// BenchmarkSelectIntSlice100 тестирует скорость выборки 100 элементов из среза.
func BenchmarkSelectIntSlice100(b *testing.B) {
	for i := 0; i < b.N; i++ {
		selectXIntSlice(100, b)
	}
}
