package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io"
	"os"
)

type Config struct {
	AppName    string `json:"AppName"`
	Production bool   `json:"Production"`
}

func main() {
	conf := Config{}

	var fileName string
	flag.StringVar(&fileName, "conf", "", "name of config file")

	flag.Parse()

	var in io.Reader

	f, err := os.Open(fileName)
	if err != nil {
		fmt.Println("error opening file: err:", err)
		os.Exit(1)
	}
	defer f.Close()
	in = f

	p := make([]byte, 1024)
	n, err := in.Read(p)
	p = p[:n]
	if err != nil {
		fmt.Println("error reading file: err:", err)
		os.Exit(1)
	}

	err = json.Unmarshal(p, &conf)
	if err != nil {
		fmt.Println("error unmarshaling json: err:", err)
		os.Exit(1)
	}

	fmt.Printf("Appname: %s\n", conf.AppName)
	fmt.Printf("Production: %t\n", conf.Production)
}

// Пример вывода конфига
// Production: true
// AppName: мое тестовое приложение
