// You can edit this code!
// Click here and start typing.
package main

import (
	"bufio"
	"bytes"
	"fmt"
	"io"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data := []string{
		"there is 3pm, but im still alive to write this code snippet",
		"чистый код лучше, чем заумный код",
		"ваш код станет наследием будущих программистов",
		"задумайтесь об этом",
	}
	// здесь расположите буфер
	buffer := new(bytes.Buffer)
	// запишите данные в буфер
	for _, str := range data {
		_, err := buffer.WriteString(str + "\n")
		check(err)
	}
	// создайте файл
	file, err := os.Create("/home/rokzish2/go/src/gitlab.com/Rokzish/go-kata/module2/stl/io/homework/example.txt")
	check(err)
	// запишите данные в файл
	_, err = io.Copy(file, buffer)
	check(err)
	// прочтите данные в новый буфе
	file, err = os.Open("/home/rokzish2/go/src/gitlab.com/Rokzish/go-kata/module2/stl/io/homework/example.txt")
	check(err)
	newBuffer := bufio.NewReader(file)

	// выведите данные из буфера buffer.String()
	for {
		line, err := newBuffer.ReadString('\n')
		if err != nil {
			if err == io.EOF {
				break
			} else {
				panic(err)
			}
		}
		fmt.Print(line)
	}
	// у вас все получится!
}
