package main

import (
	"io/ioutil"
	"os"

	"github.com/alexsergivan/transliterator"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	data, err := ioutil.ReadFile("/home/rokzish2/go/src/gitlab.com/Rokzish/go-kata/module2/stl/files/write/task2/example.txt")
	check(err)

	trans := transliterator.NewTransliterator(nil)
	translitedData := trans.Transliterate(string(data), "en")

	f, err := os.OpenFile("/home/rokzish2/go/src/gitlab.com/Rokzish/go-kata/module2/stl/files/write/task2/example.processed.txt", os.O_APPEND|os.O_WRONLY, 0600)
	check(err)
	defer f.Close()
	_, err = f.WriteString(translitedData)
	check(err)
}
