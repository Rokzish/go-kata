package main

import (
	"bufio"
	"fmt"
	"os"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {

	reader := bufio.NewReader(os.Stdin)

	fmt.Print("Enter your name: ")

	name, _ := reader.ReadString('\n')
	fmt.Printf("Hello %s\n", name)
	appendName(name)

}

func appendName(name string) {
	f, err := os.OpenFile("/home/rokzish2/go/src/gitlab.com/Rokzish/go-kata/module2/stl/files/write/task1/names.txt", os.O_APPEND|os.O_WRONLY, 0600)
	check(err)
	defer f.Close()
	_, err = f.WriteString(name)
	check(err)
}
