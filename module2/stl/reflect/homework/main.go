package main

import (
	"fmt"
	"reflect"
	"time"
)

var reflectedStructs map[string][]Field

type Field struct {
	Name string
	Tags map[string]string
}

type UserDTO struct {
	ID            int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Name          string    `json:"name" db:"name" db_type:"varchar(55)" db_default:"default null" db_ops:"create,update"`
	Phone         string    `json:"phone" db:"phone" db_type:"varchar(34)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Email         string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"default null" db_index:"index,unique" db_ops:"create,update"`
	Password      string    `json:"password" db:"password" db_type:"varchar(144)" db_default:"default null" db_ops:"create,update"`
	Status        int       `json:"status" db:"status" db_type:"int" db_default:"default 0" db_ops:"create,update"`
	Role          int       `json:"role" db:"role" db_type:"int" db_default:"not null" db_ops:"create,update"`
	Verified      bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	EmailVerified bool      `json:"email_verified" db:"email_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	PhoneVerified bool      `json:"phone_verified" db:"phone_verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt     time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	UpdatedAt     time.Time `json:"updated_at" db:"updated_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
	DeletedAt     time.Time `json:"deleted_at" db:"deleted_at" db_type:"timestamp" db_default:"default null" db_index:"index"`
}

type EmailVerifyDTO struct {
	ID        int       `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Email     string    `json:"email" db:"email" db_type:"varchar(89)" db_default:"not null" db_index:"index,unique" db_ops:"create,update"`
	UserID    int       `json:"user_id,omitempty" db:"user_id" db_ops:"create" db_type:"int" db_default:"not null" db_index:"index"`
	Hash      string    `json:"hash,omitempty" db:"hash" db_ops:"create" db_type:"char(36)" db_default:"not null" db_index:"index"`
	Verified  bool      `json:"verified" db:"verified" db_type:"boolean" db_default:"not null" db_ops:"create,update"`
	CreatedAt time.Time `json:"created_at" db:"created_at" db_type:"timestamp" db_default:"default (now()) not null" db_index:"index"`
}

func main() {
	structs := []interface{}{
		UserDTO{},
		EmailVerifyDTO{},
	}

	// заполни данными структур согласно заданиям
	reflectedStructs = make(map[string][]Field, len(structs))

	ReflectMap(structs, reflectedStructs)

	for str, m := range reflectedStructs {
		fmt.Println(str, ": ")
		for _, val := range m {
			fmt.Println(val)
		}
	}
}

func ReflectMap(s []interface{}, m map[string][]Field) {

	vm := reflect.ValueOf(m)
	for _, struc := range s {
		vs := reflect.ValueOf(struc)

		filled := fillField(struc)

		vm.SetMapIndex(reflect.ValueOf(vs.Type().Name()), filled)

	}

}

func fillField(s interface{}) reflect.Value {
	vs := reflect.ValueOf(s)
	ts := reflect.TypeOf(s)
	filled := reflect.ValueOf(make([]Field, vs.NumField()))
	for i := 0; i < vs.NumField(); i++ {
		filled.Index(i).Field(0).SetString(ts.Field(i).Name)

		tags := divideTag(string(ts.Field(0).Tag))

		filled.Index(i).Field(1).Set(reflect.ValueOf(make(map[string]string, len(tags))))
		for _, keyAndVal := range tags {

			var key string
			for _, char := range keyAndVal {
				if char == ':' {
					break
				}
				key += string(char)
			}
			filled.Index(i).Field(1).SetMapIndex(reflect.ValueOf(key), reflect.ValueOf(ts.Field(i).Tag.Get(key)))
		}

	}
	return filled
}

func divideTag(s string) []string {
	var tags []string
	f := false
	var temp string
	for _, val := range s {

		temp += string(val)

		if val == '"' && !f {
			f = true

		} else if val == '"' && f {
			f = false
			tags = append(tags, temp)
			temp = ""
		}

	}
	for i, val := range tags {
		if val[0] == ' ' {
			tags[i] = val[1:]
		}
	}
	return tags
}
