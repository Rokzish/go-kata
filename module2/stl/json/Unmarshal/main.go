package main

import (
	"encoding/json"
	"fmt"
	"strings"
)

type Person struct {
	Name string
	Age  int
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func main() {
	jsonStream := strings.NewReader(`
{"Name":"Ross Geller", "Age":28}
{"Name": "Monica Geller", "Age":27}
{"Name":"Jack Geller", "Age": 56}
`)
	decoder := json.NewDecoder(jsonStream)

	var ross, monica Person
	var jack interface{}

	err := decoder.Decode(&ross)
	check(err)
	err = decoder.Decode(&monica)
	check(err)
	err = decoder.Decode(&jack)
	check(err)

	fmt.Printf("ross: %#v\n", ross)
	fmt.Printf("monica: %#v\n", monica)
	fmt.Printf("jack: %#v\n", jack)
}
