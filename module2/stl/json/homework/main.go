package main

import (
	"encoding/json"
	"fmt"
)

type Profile struct {
	Username  string `json:"name"`
	Followers int    `json:"followers,omitempty,string"`
}
type Student struct {
	FirstName string  `json:"fname"`
	LastName  string  `json:"lname,omitempty"`
	Email     string  `json:"-"`
	Age       int     `json:"-,"`
	IsMale    bool    `json:",string"`
	Profile   Profile `json:""`
}

func main() {
	john := &Student{
		FirstName: "John",
		LastName:  "",
		Age:       21,
		Profile: Profile{
			Username:  "johndoe91",
			Followers: 1975,
		},
	}
	johnJSON, _ := json.MarshalIndent(john, "", "  ")
	fmt.Println(string(johnJSON))
}
