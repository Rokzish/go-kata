package main

import (
	"fmt"
	"unsafe"
)

func main() {

	typeInt()
}

func typeInt() {
	fmt.Println("=== START type int ===")
	var numUint8 uint8 = 1 << 7
	var min8 = int8(numUint8)
	numUint8--
	var max8 = int8(numUint8)
	fmt.Println("int8 min value:", min8, "int8 max value:", max8, "size", unsafe.Sizeof(min8), "bytes")

	var numUint16 uint16 = 1 << 15
	var min16 = int16(numUint16)
	numUint16--
	var max16 = int16(numUint16)
	fmt.Println("int16 min value:", min16, "int max value:", max16, "size", unsafe.Sizeof(min16), "bytes")

	var numUint32 uint32 = 1 << 31
	var min32 = int32(numUint32)
	numUint32--
	var max32 = int32(numUint32)
	fmt.Println("int32 min value:", min32, "int32 max value:", max32, "size", unsafe.Sizeof(min32), "bytes")

	var numUint64 uint64 = 1 << 63
	var min64 = int64(numUint64)
	numUint64--
	var max64 = int64(numUint64)
	fmt.Println("int64 min value:", min64, "int64 max value:", max64, "size", unsafe.Sizeof(min64), "bytes")

	fmt.Println("=== END type int ===")
}
