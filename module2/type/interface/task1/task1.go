package main

import (
	"fmt"
)

type MyInterface interface{}

func main() {
	var n *int
	fmt.Println(n == nil)
	test(n)
}

func test(r interface{}) {
	switch x := r.(type) {
	case *int:
		if x == nil {
			fmt.Println("Success!")
		}
	}
}

// Не совсем понял, при чем тут type-switch
