package main

import "fmt"

type Project struct {
	Name  string
	Stars int
}

func main() {
	projects := []Project{
		{
			Name:  "https://github.com/docker/compose",
			Stars: 27600,
		},
		{
			Name:  "https://github.com/sundowndev/phoneinfoga",
			Stars: 8763,
		},
		{
			Name:  "https://github.com/pocketbase/pocketbase",
			Stars: 19412,
		},
		{
			Name:  "https://github.com/m1guelpf/chatgpt-telegram",
			Stars: 2188,
		},
		{
			Name:  "https://github.com/kubeshark/kubeshark",
			Stars: 7892,
		},
		{
			Name:  "https://github.com/ThreeDotsLabs/wild-workouts-go-ddd-example",
			Stars: 3648,
		},
		{
			Name:  "https://github.com/aler9/rtsp-simple-server",
			Stars: 4832,
		},
		{
			Name:  "https://github.com/buger/goreplay",
			Stars: 16657,
		},
		{
			Name:  "https://github.com/hibiken/asynq",
			Stars: 5102,
		},
		{
			Name:  "https://github.com/syncthing/syncthing",
			Stars: 48601,
		},
		{
			Name:  "https://github.com/IceWhaleTech/CasaOS",
			Stars: 8116,
		},
		{
			Name:  "https://github.com/techschool/simplebank",
			Stars: 2086,
		},
		{
			Name:  "https://github.com/restic/restic",
			Stars: 18987,
		},
	}

	// в цикле запишите в map
	git := make(map[string]Project)
	for _, val := range projects {
		git[val.Name] = val
	}
	// в цикле пройдитесь по мапе и выведите значения в консоль
	for _, val := range git {
		fmt.Println(val)
	}

}
