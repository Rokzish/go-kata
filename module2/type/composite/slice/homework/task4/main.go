package main

import "fmt"

type User struct {
	Name string
	Age  int
}

func main() {
	users := []User{
		{
			Name: "Eva",
			Age:  13,
		},
		{
			Name: "Victor",
			Age:  28,
		},
		{
			Name: "Dex",
			Age:  34,
		},
		{
			Name: "Billy",
			Age:  21,
		},
		{
			Name: "Foster",
			Age:  29,
		},
	}
	subUsers := make([]User, len(users))
	copy(subUsers, users)
	editSecondSlice(subUsers)
	/*subUsers := users[2:]
	subUsers = editSecondSlice(subUsers)*/

	fmt.Println(users)
}
func editSecondSlice(users []User) {
	for i := range users {
		users[i].Name = "unknown"
	}
}

/*func editSecondSlice(users []User) []User {
	tmpSlice := make([]User, len(users))
	copy(tmpSlice, users)
	for i := range tmpSlice {
		tmpSlice[i].Name = "unknown"
	}
	return tmpSlice

}*/
