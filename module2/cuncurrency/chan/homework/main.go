package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

const Timeout = time.Second * 30

func joinChannels(chs ...<-chan int) chan int {
	mergedCh := make(chan int)

	go func() {
		wg := &sync.WaitGroup{}

		wg.Add(len(chs))

		for _, ch := range chs {
			go func(ch <-chan int, wg *sync.WaitGroup) {
				defer wg.Done()
				for id := range ch {
					mergedCh <- id
				}
			}(ch, wg)
		}

		wg.Wait()
		close(mergedCh)
	}()

	return mergedCh
}

func generateData(t *time.Ticker, checkCh chan struct{}) chan int {
	out := make(chan int, 1000)

	go func() {
		defer close(out)
		for {
			select {
			case <-t.C:
				close(checkCh)
				return
			default:

			}

			select {
			case <-t.C:
				close(checkCh)
				return
			case out <- rand.Intn(100):
			}
		}
	}()

	return out
}

func main() {
	timer := time.Now()
	ticker := time.NewTicker(Timeout)
	defer ticker.Stop()
	defer fmt.Println("Timeout")

	stopCh := make(chan struct{})

	rand.Seed(time.Now().UnixNano())
	a := make(chan int)
	b := make(chan int)
	c := make(chan int)

	out := generateData(ticker, stopCh)

	go func() {
		for num := range out {
			select {
			case <-stopCh:
				break
			default:

			}
			select {
			case <-stopCh:
				break
			case a <- num:
			}

		}
		close(a)
	}()

	go func() {
		for num := range out {
			select {
			case <-stopCh:
				break
			default:

			}
			select {
			case <-stopCh:
				break
			case b <- num:
			}
		}
		close(b)
	}()

	go func() {
		for num := range out {
			select {
			case <-stopCh:
				break
			default:

			}
			select {
			case <-stopCh:
				break
			case c <- num:
			}
		}
		close(c)
	}()

	mainChan := joinChannels(a, b, c)

	for num := range mainChan {
		select {
		case <-stopCh:
			break
		default:
		}
		fmt.Println(num)
	}
	fmt.Println("Time spent", time.Since(timer))

}
