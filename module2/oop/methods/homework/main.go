package main

import "fmt"

type calc struct {
	a, b   float64
	result float64
}

func NewCalc() *calc { // конструктор калькулятора
	return &calc{}
}

func (c *calc) SetA(a float64) {
	c.a = a

}

func (c *calc) SetB(b float64) {
	c.b = b
}

func (c *calc) Do(operation func(a, b float64) float64) {
	c.result = operation(c.a, c.b)

}

func (c *calc) Result() float64 {
	return c.result
}

func multiply(a, b float64) float64 { // реализуйте по примеру divide, sum, average
	return a * b
}

func divide(a, b float64) float64 {
	return a / b
}

func sum(a, b float64) float64 {
	return a + b
}

func average(a, b float64) float64 {
	return (a + b) / 2
}

func main() {
	calcx := NewCalc()
	calcx.SetA(10)
	calcx.SetB(34)
	calcx.Do(multiply)
	res := calcx.Result()
	fmt.Println(res)
	calcx.Do(divide)
	res = calcx.Result()
	fmt.Printf("%.2f", res)
	calcx.Do(sum)
	res = calcx.Result()
	fmt.Println(res)
	calcx.Do(average)
	res = calcx.Result()
	fmt.Println(res)
	res2 := calcx.Result()
	fmt.Println(res2)
	if res != res2 {
		panic("object statement is not persist")
	}
}
