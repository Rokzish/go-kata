package main

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestSum(t *testing.T) {
	type testCase struct {
		a    float64
		b    float64
		want float64
	}
	testCases := []testCase{
		{2, 3, 5},
		{2, -3, -1},
		{-5, 2, -3},
		{-4, -5, -9},
		{123.123, 321.321, 444.444},
		{123.123, -321.123, -198},
		{123321, 0.123321, 123321.123321},
		{333, -3.33, 329.67},
	}

	for _, test := range testCases {
		expectedResult := test.want
		receivedResult := sum(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
		fmt.Printf("case: a = %.2f, b = %.2f, expected result equal to %.2f, received result equal to %.2f\n", test.a, test.b, test.want, receivedResult)
	}
}
func TestDivide(t *testing.T) {
	type testCase struct {
		a    float64
		b    float64
		want float64
	}
	testCases := []testCase{
		{4, 2, 2},
		{2, -3, -0.6666666666666666},
		{-5, 2, -2.5},
		{-4, -5, 0.8},
		{123.123, 321.321, 0.38317757009345793},
		{123.123, -321.321, -0.38317757009345793},
		{123321, 0.3, 411070},
		{333, -3.33, -100},
	}

	for _, test := range testCases {
		expectedResult := test.want
		receivedResult := divide(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
		fmt.Printf("case: a = %.2f, b = %.2f, expected result equal to %.2f, received result equal to %.2f\n", test.a, test.b, test.want, receivedResult)
	}
}

func TestAverage(t *testing.T) {
	type testCase struct {
		a    float64
		b    float64
		want float64
	}
	testCases := []testCase{
		{2, 3, 2.5},
		{2, -3, -0.5},
		{-5, 2, -1.5},
		{-4, -5, -4.5},
		{12.2, -4.1, 4.05},
		{-123, 0.123, -61.4385},
		{12.21, 1232.01, 622.11},
		{202.02, -2.02, 100},
	}

	for _, test := range testCases {
		expectedResult := test.want
		receivedResult := average(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
		fmt.Printf("case: a = %.2f, b = %.2f, expected result equal to %.2f, received result equal to %.2f\n", test.a, test.b, test.want, receivedResult)
	}
}

func TestMultiply(t *testing.T) {
	type testCase struct {
		a    float64
		b    float64
		want float64
	}
	testCases := []testCase{
		{2, 3, 6},
		{2, -3, -6},
		{-5, 2, -10},
		{-4, -5, 20},
		{12.12, 32.32, 391.7184},
		{123.123, -321.123, -39537.627129},
		{123321, 0.123321, 15208.069041},
		{333, -3.33, -1108.89},
	}

	for _, test := range testCases {
		expectedResult := test.want
		receivedResult := multiply(test.a, test.b)
		assert.Equal(t, expectedResult, receivedResult)
		fmt.Printf("case: a = %.2f, b = %.2f, expected result equal to %.2f, received result equal to %.2f\n", test.a, test.b, test.want, receivedResult)
	}
}
