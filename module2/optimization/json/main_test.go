package main

import (
	"testing"
)

var jsonData []byte = []byte(`
	[
  {
    "id": 2609,
    "name": "Json Whiskers",
    "photoUrls": [
      "http://cutecats.com/1.jpg",
      "http://cutecats.com/2.jpg",
      "http://cutecats.com/3.jpg"
    ],
    "tags": [],
    "status": "pending"
  },
  {
    "id": 108,
    "category": {
      "id": 26,
      "name": "charlie"
    },
    "name": "bella",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 300,
        "name": "dogs"
      }
    ],
    "status": "pending"
  },
  {
    "id": 2883561,
    "category": {
      "id": 6527087,
      "name": "cat"
    },
    "name": "Jamie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 5899443,
        "name": "wild"
      }
    ],
    "status": "pending"
  },
  {
    "id": 7352773,
    "category": {
      "id": 6487168,
      "name": "fish"
    },
    "name": "Marie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 1303120,
        "name": "wild"
      }
    ],
    "status": "pending"
  },
  {
    "id": 9223372036854776000,
    "name": "veeresh",
    "photoUrls": [],
    "tags": [],
    "status": "pending"
  },
  {
    "id": 6683744,
    "category": {
      "id": 9741305,
      "name": "fish"
    },
    "name": "Tracy",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 6495116,
        "name": "pet"
      }
    ],
    "status": "pending"
  },
  {
    "id": 7335229,
    "category": {
      "id": 7173820,
      "name": "dog"
    },
    "name": "Sherri",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 8216422,
        "name": "pet"
      }
    ],
    "status": "pending"
  },
  {
    "id": 1525925,
    "category": {
      "id": 4351721,
      "name": "snake"
    },
    "name": "Carl",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 1540527,
        "name": "wild"
      }
    ],
    "status": "pending"
  },
  {
    "id": 7534067,
    "category": {
      "id": 7359741,
      "name": "fish"
    },
    "name": "Vicki",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 8433947,
        "name": "pet"
      }
    ],
    "status": "pending"
  },
  {
    "id": 444,
    "category": {
      "id": 555,
      "name": "dog"
    },
    "name": "Marchall",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 0,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 115,
    "category": {
      "id": 0,
      "name": "sihngo"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 117,
        "name": "samba"
      }
    ],
    "status": "pending"
  },
  {
    "id": 111,
    "category": {
      "id": 111,
      "name": "string"
    },
    "name": "doggie",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 111,
        "name": "string"
      }
    ],
    "status": "pending"
  },
  {
    "id": 927552263476184200,
    "category": {
      "id": 1938955188024489500,
      "name": "Emmett Wisozk"
    },
    "name": "doggie",
    "photoUrls": [
      "voluptatem"
    ],
    "tags": [
      {
        "id": 1865631746881124000,
        "name": "Cornelius Shields Jr."
      }
    ],
    "status": "pending"
  },
  {
    "id": 67,
    "category": {
      "id": 1,
      "name": "Cat"
    },
    "name": "wanda",
    "photoUrls": [
      "string"
    ],
    "tags": [
      {
        "id": 1,
        "name": "tyu"
      }
    ],
    "status": "pending"
  },
  {
    "id": 5441969035401344000,
    "category": {
      "id": 2426659689868645400,
      "name": "Becky Keeling"
    },
    "name": "doggie",
    "photoUrls": [
      "voluptatem"
    ],
    "tags": [
      {
        "id": 166646215430820830,
        "name": "Salvatore Rutherford"
      }
    ],
    "status": "pending"
  },
  {
    "id": 4895358982099693000,
    "category": {
      "id": 3103720028039821000,
      "name": "Lewis Marks"
    },
    "name": "doggie",
    "photoUrls": [
      "blanditiis"
    ],
    "tags": [
      {
        "id": 1679965528464657700,
        "name": "Dora Ankunding"
      }
    ],
    "status": "pending"
  },
  {
    "id": 123,
    "category": {
      "id": 123,
      "name": "Pet shop"
    },
    "name": "bobica",
    "photoUrls": [
      "https://www.google.com/url?sa=i&url=https%3A%2F%2Fm.facebook.com%2F645593892227945%2Fphotos%2Fwhitedogcute%2F2260400187413966%2F&psig=AOvVaw3yniQNlL6t52MhcE0dITOY&ust=1677674086017000&source=images&cd=vfe&ved=0CA8QjRxqFwoTCNCO7pCduP0CFQAAAAAdAAAAABAE"
    ],
    "tags": [
      {
        "id": 123,
        "name": "bobica"
      }
    ],
    "status": "pending"
  },
  {
    "id": 31662904,
    "category": {
      "id": -72976378,
      "name": "consequat commodo dolor"
    },
    "name": "doggie",
    "photoUrls": [
      "dolor",
      "aliquip"
    ],
    "tags": [
      {
        "id": -81536039,
        "name": "officia non exercitation laborum"
      },
      {
        "id": -81700539,
        "name": "sed quis"
      }
    ],
    "status": "pending"
  },
  {
    "id": 11913632,
    "category": {
      "id": -36770290,
      "name": "consequat veniam"
    },
    "name": "doggie",
    "photoUrls": [
      "nisi et do qui nostrud",
      "et incididunt"
    ],
    "tags": [
      {
        "id": 4928386,
        "name": "sit ea"
      },
      {
        "id": 79149242,
        "name": "ullamco consectetur in"
      }
    ],
    "status": "pending"
  }
]
`)

func BenchmarkMarshalStandard(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	pets, err = UnmarshalPets(jsonData)
	if err != nil {
		panic(err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal()
		if err != nil {
			panic(err)
		}
		_ = data
	}

}

func BenchmarkMarshalJsonIter(b *testing.B) {
	var (
		pets Pets
		err  error
		data []byte
	)
	pets, err = UnmarshalPets(jsonData)
	if err != nil {
		panic(err)
	}
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		data, err = pets.Marshal2()
		if err != nil {
			panic(err)
		}
		_ = data
	}
}

func BenchmarkUnmarshalStandard(b *testing.B) {
	var (
		pets Pets
		err  error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets(jsonData)
		if err != nil {
			panic(err)
		}
		_ = pets
	}
}

func BenchmarkUnmarshalJsonIter(b *testing.B) {
	var (
		pets Pets
		err  error
	)
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		pets, err = UnmarshalPets2(jsonData)
		if err != nil {
			panic(err)
		}
		_ = pets
	}

}
