package main

import "testing"

func BenchmarkSanitizeText(b *testing.B) {
	for i := 0; i < b.N; i++ {
		for i := range data {
			SanitizeText(data[i])
		}
	}
}

func BenchmarkSanitizeText2(b *testing.B) {
	fr := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			fr.SanitizeText2(data[i])
		}
	}
}

func BenchmarkSanitizeText3(b *testing.B) {
	fr := NewFilterText()
	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		for i := range data {
			fr.SanitizeText3(data[i])
		}
	}
}
