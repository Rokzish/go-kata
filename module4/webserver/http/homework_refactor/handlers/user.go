package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/Rokzish/go-kata/module4/webserver/http/homework_refactor/domain/entity"
)

type UserService interface {
	GetUsersByID(id string) (*entity.User, error)
	GetAllUsers() ([]*entity.User, error)
	CreateUser(user *entity.User) error
}

type UserHandler struct {
	userService UserService
}

func NewUserUserHandler(userService UserService) *UserHandler {
	return &UserHandler{userService: userService}
}

func (h *UserHandler) GetUser(w http.ResponseWriter, r *http.Request) {

	var (
		user   *entity.User
		userID string
		err    error
	)
	userID = chi.URLParam(r, "userID")

	user, err = h.userService.GetUsersByID(userID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(*user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *UserHandler) GeListUsers(w http.ResponseWriter, r *http.Request) {
	var (
		users []*entity.User
		err   error
		list  []entity.User
	)
	users, err = h.userService.GetAllUsers()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	if len(users) < 1 {
		_ = json.NewEncoder(w).Encode([]byte("User list is empty."))
		return
	}
	for _, user := range users {
		list = append(list, *user)
	}

	err = json.NewEncoder(w).Encode(list)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *UserHandler) CreateUser(w http.ResponseWriter, r *http.Request) {
	var (
		user entity.User
		err  error
	)

	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = h.userService.CreateUser(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
