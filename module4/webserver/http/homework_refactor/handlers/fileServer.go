package handlers

import (
	"io"
	"mime/multipart"
	"net/http"
	"os"
)

const fileStorageDirPath = "./public/"

func UploadFile(w http.ResponseWriter, r *http.Request) {
	var (
		file     multipart.File
		header   *multipart.FileHeader
		err      error
		tempFile *os.File
	)
	_ = r.ParseMultipartForm(10 << 20)
	file, header, err = r.FormFile("uploadedFile")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer file.Close()
	tempFile, err = os.Create(fileStorageDirPath + header.Filename)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
	defer tempFile.Close()
	_, err = io.Copy(tempFile, file)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}

}

func GetFile(w http.ResponseWriter, r *http.Request) {

	http.ServeFile(w, r, "."+r.URL.Path)

}
