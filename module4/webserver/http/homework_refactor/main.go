package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/render"
	"gitlab.com/Rokzish/go-kata/module4/webserver/http/homework_refactor/domain/service"
	"gitlab.com/Rokzish/go-kata/module4/webserver/http/homework_refactor/handlers"
	"gitlab.com/Rokzish/go-kata/module4/webserver/http/homework_refactor/repository"
)

func main() {
	port := ":8080"
	repoPath := "./http_storage.json"
	userService := service.NewUserService(repository.NewUserStorage(repoPath))
	handler := handlers.NewUserUserHandler(userService)

	server := &http.Server{Addr: port, Handler: newRouter(handler)}
	serverCtx, serverStopCtx := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, os.Interrupt)

	go func() {
		<-sig
		shutdownCtx, cancel := context.WithTimeout(serverCtx, 5*time.Second)
		defer cancel()
		log.Println("Shutdown Server ...")
		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("graceful shutdown timed out... forcing exit.")
			}
		}()
		err := server.Shutdown(shutdownCtx)
		if err != nil {
			log.Fatal(err)
		}
		serverStopCtx()
	}()
	log.Println("Server started on port", port)
	err := server.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}

	<-serverCtx.Done()
	log.Println("Server exiting")
}

func newRouter(handler *handlers.UserHandler) http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middleware.URLFormat)
	r.Use(render.SetContentType(render.ContentTypeJSON))

	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		_, _ = w.Write([]byte("Welcome message"))
	})

	r.Route("/users", func(r chi.Router) {
		r.Get("/", handler.GeListUsers)
		r.Post("/", handler.CreateUser)
		r.Get("/:{userID}", handler.GetUser)

	})

	r.Post("/upload", handlers.UploadFile)
	r.Route("/public", func(r chi.Router) {
		r.Get("/*", handlers.GetFile)
	})
	return r
}
