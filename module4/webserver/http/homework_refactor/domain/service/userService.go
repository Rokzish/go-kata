package service

import (
	"gitlab.com/Rokzish/go-kata/module4/webserver/http/homework_refactor/domain/entity"
)

type UserStorage interface {
	GetByID(id string) (*entity.User, error)
	GetAllUser() ([]*entity.User, error)
	CreateUser(user *entity.User) error
}

type userService struct {
	storage UserStorage
}

func NewUserService(storage UserStorage) *userService {
	return &userService{storage: storage}
}

func (s *userService) GetUsersByID(id string) (*entity.User, error) {
	return s.storage.GetByID(id)
}

func (s *userService) GetAllUsers() ([]*entity.User, error) {
	return s.storage.GetAllUser()
}

func (s *userService) CreateUser(user *entity.User) error {
	return s.storage.CreateUser(user)
}
