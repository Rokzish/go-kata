package repository

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"os"

	"gitlab.com/Rokzish/go-kata/module4/webserver/http/homework_refactor/domain/entity"
)

type UserStorage struct {
	filePath string
}

func NewUserStorage(path string) *UserStorage {
	return &UserStorage{filePath: path}
}

func (s *UserStorage) GetByID(id string) (*entity.User, error) {
	usersList, err := s.GetAllUser()
	if err != nil {
		return nil, err
	}

	for _, u := range usersList {
		if id == u.ID {
			return u, nil
		}
	}

	return nil, errors.New("user not found")
}

func (s *UserStorage) GetAllUser() ([]*entity.User, error) {
	userList := []*entity.User{}
	file, err := os.OpenFile(s.filePath, os.O_RDONLY, 0755)
	if err != nil {
		return nil, err
	}
	defer file.Close()
	content, err := ioutil.ReadAll(file)
	if err != nil {
		return nil, err
	}
	if len(content) > 0 {
		if err := json.Unmarshal(content, &userList); err != nil {
			return nil, err
		}
	}
	return userList, nil

}

func (s *UserStorage) CreateUser(user *entity.User) error {
	usersList, err := s.GetAllUser()
	if err != nil {
		return err
	}
	usersList = append(usersList, user)

	if err := s.saveUsers(usersList); err != nil {
		return err
	}
	return nil
}

func (s *UserStorage) saveUsers(userList []*entity.User) error {
	file, err := os.OpenFile(s.filePath, os.O_WRONLY, 0755)
	if err != nil {
		return err
	}
	defer file.Close()
	for i := 0; i < len(userList); i++ {
		userList[i].ID = fmt.Sprintf("%d", i)
	}

	toSave, err := json.MarshalIndent(userList, "", " ")
	if err != nil {
		return err
	}

	err = file.Truncate(0)
	if err != nil {
		return err
	}
	if _, err = file.Write(toSave); err != nil {
		return err
	}

	return nil

}
