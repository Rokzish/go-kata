package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"
	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

type StoreService interface {
	CreateOrder(order entity.Order) entity.Order
	GetOrderByID(orderID int) (entity.Order, error)
	DeleteOrder(orderID int) error
	GetStoreStatus() map[string]int
}

type StoreHandler struct {
	storeService StoreService
}

func NewStoreHandler(storeService StoreService) *StoreHandler {
	return &StoreHandler{storeService: storeService}
}
func (s *StoreHandler) StorePostCreate(w http.ResponseWriter, r *http.Request) {
	var order entity.Order
	err := json.NewDecoder(r.Body).Decode(&order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order = s.storeService.CreateOrder(order)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreHandler) StoreGetByID(w http.ResponseWriter, r *http.Request) {
	var (
		order      entity.Order
		err        error
		orderIDRaw string
		orderID    int
	)

	orderIDRaw = chi.URLParam(r, "orderID")

	orderID, err = strconv.Atoi(orderIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	order, err = s.storeService.GetOrderByID(orderID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(order)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (s *StoreHandler) StoreDeleteByID(w http.ResponseWriter, r *http.Request) {
	var (
		err        error
		orderIDRaw string
		orderID    int
	)

	orderIDRaw = chi.URLParam(r, "orderID")
	orderID, err = strconv.Atoi(orderIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = s.storeService.DeleteOrder(orderID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)

		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	out := ApiResponse{
		Code:    200,
		Message: orderIDRaw,
	}
	err = json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (s *StoreHandler) StoreGetInventory(w http.ResponseWriter, r *http.Request) {
	var statusMap map[string]int
	var err error
	statusMap = s.storeService.GetStoreStatus()
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(statusMap)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
