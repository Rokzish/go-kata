package handlers

import (
	"encoding/json"
	"net/http"

	"github.com/go-chi/chi/v5"
	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

type UserService interface {
	CreateUser(user entity.User)
	GetUserByName(username string) (entity.User, error)
	UpdateUserByName(username string, user entity.User) error
	DeleteUserByName(username string) error
}

type UserHandler struct {
	userService UserService
}

func NewUserHandler(userService UserService) *UserHandler {
	return &UserHandler{userService: userService}
}

func (u *UserHandler) UserCreateList(w http.ResponseWriter, r *http.Request) {
	var (
		users []entity.User
		err   error
	)
	err = json.NewDecoder(r.Body).Decode(&users)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	for _, user := range users {
		u.userService.CreateUser(user)
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	out := ApiResponse{
		Code:    200,
		Message: "success",
	}
	err = json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (u *UserHandler) UserGetByName(w http.ResponseWriter, r *http.Request) {
	var (
		user     entity.User
		err      error
		username string
	)

	username = chi.URLParam(r, "username")

	user, err = u.userService.GetUserByName(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserHandler) UserUpdate(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		user     entity.User
		username string
	)
	username = chi.URLParam(r, "username")
	err = json.NewDecoder(r.Body).Decode(&user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = u.userService.UpdateUserByName(username, user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	out := ApiResponse{
		Code:    200,
		Message: "success",
	}
	err = json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserHandler) UserDeleteByName(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		username string
	)

	username = chi.URLParam(r, "username")
	err = u.userService.DeleteUserByName(username)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	out := ApiResponse{
		Code:    200,
		Message: username,
	}
	err = json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (u *UserHandler) UserCreate(w http.ResponseWriter, r *http.Request) {
	var (
		user entity.User
		err  error
	)
	err = json.NewDecoder(r.Body).Decode(&user)

	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	u.userService.CreateUser(user)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	out := ApiResponse{
		Code:    200,
		Message: "success",
	}
	err = json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
