package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"github.com/go-chi/chi/v5"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

type PetService interface {
	CreatePet(pet entity.Pet) entity.Pet
	UpdatePet(pet entity.Pet) (entity.Pet, error)
	DeletePet(petID int) error
	GetPetByID(petID int) (entity.Pet, error)
	GetPetList() []entity.Pet
}

type PetHandler struct {
	petService PetService
}

func NewPetHandler(petService PetService) *PetHandler {
	return &PetHandler{petService: petService}
}

func (h *PetHandler) PetCreate(w http.ResponseWriter, r *http.Request) {
	var pet entity.Pet
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	pet = h.petService.CreatePet(pet)

	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *PetHandler) PetGetByID(w http.ResponseWriter, r *http.Request) {
	var (
		pet      entity.Pet
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID")

	petID, err = strconv.Atoi(petIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet, err = h.petService.GetPetByID(petID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *PetHandler) PetDelete(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID")
	petID, err = strconv.Atoi(petIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = h.petService.DeletePet(petID)

	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")

	out := ApiResponse{
		Code:    200,
		Message: petIDRaw,
	}
	err = json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *PetHandler) PetUpdate(w http.ResponseWriter, r *http.Request) {
	var (
		err error
		pet entity.Pet
	)
	err = json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	out, err := h.petService.UpdatePet(pet)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *PetHandler) PetFindByStatus(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	status := params["Status"][0]

	list := h.petService.GetPetList()
	out := make([]entity.Pet, 0)
	for _, pet := range list {
		if pet.Status == status {
			out = append(out, pet)
		}
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(out)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (h *PetHandler) PetUpdateForm(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		petIDRaw string
		petID    int
	)

	petIDRaw = chi.URLParam(r, "petID")
	petID, err = strconv.Atoi(petIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet, err := h.petService.GetPetByID(petID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}
	params := r.URL.Query()
	pet.Name = params["name"][0]
	pet.Status = params["status"][0]
	pet, _ = h.petService.UpdatePet(pet)

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (h *PetHandler) PetUpdateImg(w http.ResponseWriter, r *http.Request) {
	var (
		err      error
		petIDRaw string
		petID    int
	)

	_ = r.ParseMultipartForm(10 << 20)
	_, header, err := r.FormFile("formFile")
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	petIDRaw = chi.URLParam(r, "petID")
	petID, err = strconv.Atoi(petIDRaw)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	pet, err := h.petService.GetPetByID(petID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	pet.PhotoUrls = append(pet.PhotoUrls, header.Filename)

	pet, _ = h.petService.UpdatePet(pet)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(pet)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}
