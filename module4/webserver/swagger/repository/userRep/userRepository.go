package userRep

import (
	"fmt"
	"sync"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

type UserStorage struct {
	data               []*entity.User
	primaryKeyName     map[string]*entity.User
	autoIncrementCount int
	sync.Mutex
}

func NewUserStorage() *UserStorage {
	return &UserStorage{
		data:               make([]*entity.User, 0, 15),
		primaryKeyName:     make(map[string]*entity.User, 15),
		autoIncrementCount: 1,
		Mutex:              sync.Mutex{},
	}
}
func (u *UserStorage) Create(user entity.User) {
	u.Lock()
	defer u.Unlock()
	user.ID = u.autoIncrementCount
	u.primaryKeyName[user.Username] = &user
	u.autoIncrementCount++
	u.data = append(u.data, &user)
}

func (u *UserStorage) GetUserByName(username string) (entity.User, error) {
	if user, ok := u.primaryKeyName[username]; ok {
		return *user, nil
	}
	return entity.User{}, fmt.Errorf("user not found")

}

func (u *UserStorage) UpdateUserByName(username string, user entity.User) error {
	u.Lock()
	defer u.Unlock()
	oldUser, ok := u.primaryKeyName[username]
	if !ok {
		return fmt.Errorf("user to update not found")
	}
	delete(u.primaryKeyName, oldUser.Username)
	id := oldUser.ID
	user.ID = id
	u.primaryKeyName[user.Username] = &user
	u.data[id-1] = &user

	return nil

}

func (u *UserStorage) DeleteUserByName(username string) error {
	u.Lock()
	defer u.Unlock()
	oldUser, ok := u.primaryKeyName[username]
	if !ok {
		return fmt.Errorf("user to delete not found")
	}
	users := append(u.data[:oldUser.ID-1], u.data[oldUser.ID:]...)
	newPrimaryKey := make(map[string]*entity.User)
	for i, u := range users {
		users[i].ID = i + 1
		newPrimaryKey[u.Username] = u
	}
	u.data = users
	u.primaryKeyName = newPrimaryKey
	u.autoIncrementCount--
	return nil

}
