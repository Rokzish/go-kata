package userRep

import (
	"reflect"
	"testing"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

func TestUserStorage_Create(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyName     map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		user entity.User
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.User
	}{
		{
			name: "first test ",
			fields: fields{
				data:               make([]*entity.User, 0, 5),
				primaryKeyName:     make(map[string]*entity.User),
				autoIncrementCount: 1,
			},
			args: args{entity.User{
				ID:         0,
				Username:   "test name",
				FirstName:  "somethings",
				LastName:   "somethings",
				Email:      "somethings",
				Password:   "somethings",
				Phone:      "somethings",
				UserStatus: 1,
			}},
			want: entity.User{
				ID:         1,
				Username:   "test name",
				FirstName:  "somethings",
				LastName:   "somethings",
				Email:      "somethings",
				Password:   "somethings",
				Phone:      "somethings",
				UserStatus: 1,
			},
		},
		{
			name: "second test",
			fields: fields{
				data: []*entity.User{
					{
						ID:         1,
						Username:   "name 1",
						FirstName:  "somethings 1",
						LastName:   "somethings 1",
						Email:      "somethings 1",
						Password:   "somethings 1",
						Phone:      "somethings 1",
						UserStatus: 1,
					},
					{
						ID:         2,
						Username:   "name 2",
						FirstName:  "somethings 2",
						LastName:   "somethings 2",
						Email:      "somethings 2",
						Password:   "somethings 2",
						Phone:      "somethings 2",
						UserStatus: 2,
					},
					{
						ID:         3,
						Username:   "name 3",
						FirstName:  "somethings 3",
						LastName:   "somethings 3",
						Email:      "somethings 3",
						Password:   "somethings 3",
						Phone:      "somethings 3",
						UserStatus: 3,
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name 1": {
						ID:         1,
						Username:   "name 1",
						FirstName:  "somethings 1",
						LastName:   "somethings 1",
						Email:      "somethings 1",
						Password:   "somethings 1",
						Phone:      "somethings 1",
						UserStatus: 1,
					},
					"name 2": {
						ID:         2,
						Username:   "name 2",
						FirstName:  "somethings 2",
						LastName:   "somethings 2",
						Email:      "somethings 2",
						Password:   "somethings 2",
						Phone:      "somethings 2",
						UserStatus: 2,
					},
					"name 3": {
						ID:         3,
						Username:   "name 3",
						FirstName:  "somethings 3",
						LastName:   "somethings 3",
						Email:      "somethings 3",
						Password:   "somethings 3",
						Phone:      "somethings 3",
						UserStatus: 3,
					},
				},
				autoIncrementCount: 4,
			},
			args: args{entity.User{
				ID:         0,
				Username:   "new",
				FirstName:  "new",
				LastName:   "new",
				Email:      "new",
				Password:   "new",
				Phone:      "new",
				UserStatus: 0,
			}},
			want: entity.User{
				ID:         4,
				Username:   "new",
				FirstName:  "new",
				LastName:   "new",
				Email:      "new",
				Password:   "new",
				Phone:      "new",
				UserStatus: 0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := NewUserStorage()
			u.data = tt.fields.data
			u.primaryKeyName = tt.fields.primaryKeyName
			u.autoIncrementCount = tt.fields.autoIncrementCount
			var (
				got entity.User
				err error
			)

			u.Create(tt.args.user)
			if got, err = u.GetUserByName(tt.args.user.Username); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestUserStorage_DeleteUserByName(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyName     map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name": {
						ID:       1,
						Username: "name",
					},
				},
				autoIncrementCount: 2,
			},
			args:    args{"name"},
			wantErr: false,
		},
		{
			name: "second test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name 1",
					},
					{
						ID:       2,
						Username: "name 2",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name 1": {
						ID:       1,
						Username: "name 1",
					},
					"name 2": {
						ID:       2,
						Username: "name 2",
					},
				},
				autoIncrementCount: 3,
			},
			args:    args{"name 2"},
			wantErr: false,
		},
		{
			name: "third test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name 1",
					},
					{
						ID:       2,
						Username: "name 2",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name 1": {
						ID:       1,
						Username: "name 1",
					},
					"name 2": {
						ID:       2,
						Username: "name 2",
					},
				},
				autoIncrementCount: 3,
			},
			args:    args{"name"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := NewUserStorage()
			u.data = tt.fields.data
			u.primaryKeyName = tt.fields.primaryKeyName
			u.autoIncrementCount = tt.fields.autoIncrementCount
			if err := u.DeleteUserByName(tt.args.name); (err != nil) != tt.wantErr {
				t.Errorf("DeleteUserByName() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestUserStorage_GetUserByName(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyName     map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		name string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.User
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name": {
						ID:       1,
						Username: "name",
					},
				},
				autoIncrementCount: 2,
			},
			args: args{"name"},
			want: entity.User{
				ID:       1,
				Username: "name",
			},
			wantErr: false,
		},
		{
			name: "secondtest",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name 1",
					},
					{
						ID:       2,
						Username: "name 2",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name 1": {
						ID:       1,
						Username: "name 1",
					},
					"name 2": {
						ID:       2,
						Username: "name 2",
					},
				},
				autoIncrementCount: 3,
			},
			args: args{"name 2"},
			want: entity.User{
				ID:       2,
				Username: "name 2",
			},
			wantErr: false,
		},
		{
			name: "third  test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name 1",
					},
					{
						ID:       2,
						Username: "name 2",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name 1": {
						ID:       1,
						Username: "name 1",
					},
					"name 2": {
						ID:       2,
						Username: "name 2",
					},
				},
				autoIncrementCount: 3,
			},
			args:    args{"name"},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := NewUserStorage()
			u.data = tt.fields.data
			u.primaryKeyName = tt.fields.primaryKeyName
			u.autoIncrementCount = tt.fields.autoIncrementCount
			got, err := u.GetUserByName(tt.args.name)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUserByName() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetUserByName() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestUserStorage_UpdateUserByName(t *testing.T) {
	type fields struct {
		data               []*entity.User
		primaryKeyName     map[string]*entity.User
		autoIncrementCount int
	}
	type args struct {
		name string
		user entity.User
	}
	tests := []struct {
		name     string
		fields   fields
		args     args
		want     entity.User
		wantErr  bool
		wantErr2 bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name": {
						ID:       1,
						Username: "name",
					},
				},
				autoIncrementCount: 2,
			},
			args: args{"name", entity.User{
				ID:         0,
				Username:   "name",
				FirstName:  "some",
				LastName:   "some",
				Email:      "some",
				Password:   "some",
				Phone:      "some",
				UserStatus: 12,
			}},
			want: entity.User{
				ID:         1,
				Username:   "name",
				FirstName:  "some",
				LastName:   "some",
				Email:      "some",
				Password:   "some",
				Phone:      "some",
				UserStatus: 12,
			},
			wantErr:  false,
			wantErr2: false,
		},
		{
			name: "second test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name 1",
					},
					{
						ID:       2,
						Username: "name 2",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name 1": {
						ID:       1,
						Username: "name 1",
					},
					"name 2": {
						ID:       2,
						Username: "name 2",
					},
				},
				autoIncrementCount: 2,
			},
			args: args{"name 2", entity.User{
				ID:         2,
				Username:   "new name",
				FirstName:  "some",
				LastName:   "some",
				Email:      "some",
				Password:   "some",
				Phone:      "some",
				UserStatus: 1,
			}},
			want: entity.User{
				ID:         2,
				Username:   "new name",
				FirstName:  "some",
				LastName:   "some",
				Email:      "some",
				Password:   "some",
				Phone:      "some",
				UserStatus: 1,
			},
			wantErr:  false,
			wantErr2: false,
		},
		{
			name: "third test",
			fields: fields{
				data: []*entity.User{
					{
						ID:       1,
						Username: "name",
					},
				},
				primaryKeyName: map[string]*entity.User{
					"name": {
						ID:       1,
						Username: "name",
					},
				},
				autoIncrementCount: 1,
			},
			args:     args{"some", entity.User{}},
			want:     entity.User{},
			wantErr:  true,
			wantErr2: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			u := NewUserStorage()
			u.data = tt.fields.data
			u.primaryKeyName = tt.fields.primaryKeyName
			u.autoIncrementCount = tt.fields.autoIncrementCount
			var (
				got entity.User
				err error
			)
			if err = u.UpdateUserByName(tt.args.name, tt.args.user); (err != nil) != tt.wantErr {
				t.Errorf("UpdateUserByName() error = %v, wantErr %v", err, tt.wantErr)
			}
			if got, err = u.GetUserByName(tt.args.user.Username); (err != nil) != tt.wantErr2 || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("UpdateUserByName() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}
