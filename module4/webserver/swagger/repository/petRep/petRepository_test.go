package petRep

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

func TestPetStorage_Create(t *testing.T) {
	type fields struct {
		//data               []Pet
		//primaryKeyIDx      map[int64]Pet
		//autoIncrementCount int64
		//Mutex              sync.Mutex
	}
	type args struct {
		pet entity.Pet
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Pet
	}{
		{
			name: "first test",
			args: args{
				pet: entity.Pet{
					ID: 0,
					Category: entity.Category{
						ID:   0,
						Name: "test category",
					},
					Name: "Alma",
					PhotoUrls: []string{
						"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
						"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
						"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
					},
					Tags:   []entity.Category{},
					Status: "active",
				},
			},
			want: entity.Pet{
				ID: 0,
				Category: entity.Category{
					ID:   0,
					Name: "test category",
				},
				Name: "Alma",
				PhotoUrls: []string{
					"https://sobakovod.club/uploads/posts/2021-12/1639834742_26-sobakovod-club-p-sobaki-laika-chernaya-29.jpg",
					"http://catfishes.ru/wp-content/uploads/2021/06/ruslaika1.jpg",
					"https://lapkins.ru/upload/iblock/ebd/ebd19f44cd131f425feed81f9b578198.jpg",
				},
				Tags:   []entity.Category{},
				Status: "active",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var got entity.Pet
			var err error
			got = p.Create(tt.args.pet)
			tt.want.ID = got.ID // fix autoincrement value
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
			if got, err = p.GetByID(got.ID); err != nil || !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v, err %s", got, tt.want, err)
			}
		})
	}
}

func TestPetStorage_Update(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int]*entity.Pet
		autoIncrementCount int
	}
	type args struct {
		pet entity.Pet
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Pet
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.Pet{
					{
						ID:        1,
						Category:  entity.Category{ID: 0, Name: "category name"},
						Name:      "name 0",
						PhotoUrls: []string{"/pets/0/photo/0", "/pets/0/photo/1", "/pets/0/photo/2"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 0",
					},
					{
						ID:        2,
						Category:  entity.Category{ID: 1, Name: "category name"},
						Name:      "name 1",
						PhotoUrls: []string{"/pets/1/photo/0", "/pets/1/photo/1", "/pets/1/photo/2"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 0",
					},
					{
						ID:        3,
						Category:  entity.Category{ID: 2, Name: "category name"},
						Name:      "name 2",
						PhotoUrls: []string{"/pets/2/photo/0", "/pets/2/photo/1", "/pets/2/photo/2"},
						Tags:      []entity.Category{{ID: 321, Name: "some tags"}, {ID: 654, Name: "some tags"}},
						Status:    "status 0",
					},
				},
				primaryKeyIDx: map[int]*entity.Pet{
					0: {
						ID:        1,
						Category:  entity.Category{ID: 0, Name: "category name"},
						Name:      "name 0",
						PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 0",
					},
					1: {
						ID:        2,
						Category:  entity.Category{ID: 1, Name: "category name"},
						Name:      "name 1",
						PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 1",
					},
					2: {
						ID:        3,
						Category:  entity.Category{ID: 2, Name: "category name"},
						Name:      "name 2",
						PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3"},
						Tags:      []entity.Category{{ID: 321, Name: "some tags"}, {ID: 654, Name: "some tags"}},
						Status:    "status 2",
					},
				},
				autoIncrementCount: 4,
			},
			args: args{entity.Pet{
				ID:        2,
				Category:  entity.Category{ID: 1, Name: "new category name"},
				Name:      "name 1",
				PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3", "photoURL new"},
				Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
				Status:    "status new",
			}},
			want: entity.Pet{
				ID:        2,
				Category:  entity.Category{ID: 1, Name: "new category name"},
				Name:      "name 1",
				PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3", "photoURL new"},
				Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
				Status:    "status new",
			},
			wantErr: false,
		},
		{
			name: "second test",
			fields: fields{
				data: []*entity.Pet{
					{
						ID:        1,
						Category:  entity.Category{ID: 0, Name: "category name"},
						Name:      "name 0",
						PhotoUrls: []string{"/pets/0/photo/0", "/pets/0/photo/1", "/pets/0/photo/2"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 0",
					},
					{
						ID:        2,
						Category:  entity.Category{ID: 1, Name: "category name"},
						Name:      "name 1",
						PhotoUrls: []string{"/pets/1/photo/0", "/pets/1/photo/1", "/pets/1/photo/2"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 1",
					},
					{
						ID:        3,
						Category:  entity.Category{ID: 2, Name: "category name"},
						Name:      "name 2",
						PhotoUrls: []string{"/pets/2/photo/0", "/pets/2/photo/1", "/pets/2/photo/2"},
						Tags:      []entity.Category{{ID: 321, Name: "some tags"}, {ID: 654, Name: "some tags"}},
						Status:    "status 2",
					},
				},
				primaryKeyIDx: map[int]*entity.Pet{
					1: {
						ID:        1,
						Category:  entity.Category{ID: 0, Name: "category name"},
						Name:      "name 0",
						PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 0",
					},
					2: {
						ID:        2,
						Category:  entity.Category{ID: 1, Name: "category name"},
						Name:      "name 1",
						PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 1",
					},
					3: {
						ID:        3,
						Category:  entity.Category{ID: 2, Name: "category name"},
						Name:      "name 2",
						PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3"},
						Tags:      []entity.Category{{ID: 321, Name: "some tags"}, {ID: 654, Name: "some tags"}},
						Status:    "status 2",
					},
				},
				autoIncrementCount: 4,
			},
			args: args{entity.Pet{
				ID:        5,
				Category:  entity.Category{ID: 3, Name: "new category name"},
				Name:      "name 3",
				PhotoUrls: []string{"photoURL 1", "photoURL 2", "photoURL 3", "photoURL new"},
				Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
				Status:    "status new",
			}},
			want:    entity.Pet{},
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var got entity.Pet
			var err error
			p.data = tt.fields.data
			p.primaryKeyIDx = tt.fields.primaryKeyIDx
			p.autoIncrementCount = tt.fields.autoIncrementCount

			got, err = p.Update(tt.args.pet)
			if (err != nil) != tt.wantErr {
				t.Errorf("Update() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Update() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_GetList(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int]*entity.Pet
		autoIncrementCount int
	}
	tests := []struct {
		name   string
		fields fields
		want   []entity.Pet
	}{
		{
			name: "test 1",
			fields: fields{
				data: []*entity.Pet{
					{
						ID:        1,
						Category:  entity.Category{ID: 0, Name: "category name"},
						Name:      "name 0",
						PhotoUrls: []string{"/pets/0/photo/0", "/pets/0/photo/1", "/pets/0/photo/2"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 0",
					},
					{
						ID:        2,
						Category:  entity.Category{ID: 1, Name: "category name"},
						Name:      "name 1",
						PhotoUrls: []string{"/pets/1/photo/0", "/pets/1/photo/1", "/pets/1/photo/2"},
						Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
						Status:    "status 0",
					},
					{
						ID:        3,
						Category:  entity.Category{ID: 2, Name: "category name"},
						Name:      "name 2",
						PhotoUrls: []string{"/pets/2/photo/0", "/pets/2/photo/1", "/pets/2/photo/2"},
						Tags:      []entity.Category{{ID: 321, Name: "some tags"}, {ID: 654, Name: "some tags"}},
						Status:    "status 0",
					},
				},
				primaryKeyIDx:      nil,
				autoIncrementCount: 4,
			},
			want: []entity.Pet{
				{
					ID:        1,
					Category:  entity.Category{ID: 0, Name: "category name"},
					Name:      "name 0",
					PhotoUrls: []string{"/pets/0/photo/0", "/pets/0/photo/1", "/pets/0/photo/2"},
					Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
					Status:    "status 0",
				},
				{
					ID:        2,
					Category:  entity.Category{ID: 1, Name: "category name"},
					Name:      "name 1",
					PhotoUrls: []string{"/pets/1/photo/0", "/pets/1/photo/1", "/pets/1/photo/2"},
					Tags:      []entity.Category{{ID: 123, Name: "some tags"}, {ID: 456, Name: "some tags"}},
					Status:    "status 0",
				},
				{
					ID:        3,
					Category:  entity.Category{ID: 2, Name: "category name"},
					Name:      "name 2",
					PhotoUrls: []string{"/pets/2/photo/0", "/pets/2/photo/1", "/pets/2/photo/2"},
					Tags:      []entity.Category{{ID: 321, Name: "some tags"}, {ID: 654, Name: "some tags"}},
					Status:    "status 0",
				},
			},
		},
		{
			name: "test 2",
			fields: fields{
				data:               []*entity.Pet{},
				primaryKeyIDx:      nil,
				autoIncrementCount: 1,
			},
			want: make([]entity.Pet, 0),
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var got []entity.Pet
			p.data = tt.fields.data
			p.primaryKeyIDx = tt.fields.primaryKeyIDx
			p.autoIncrementCount = tt.fields.autoIncrementCount
			if got = p.GetList(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetList() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestPetStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*entity.Pet
		primaryKeyIDx      map[int]*entity.Pet
		autoIncrementCount int
	}
	type args struct {
		petID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		want    []entity.Pet
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.Pet{
					{
						ID:   1,
						Name: "Name 0",
					},
					{
						ID:   2,
						Name: "Name 1",
					},
					{
						ID:   3,
						Name: "Name 2",
					},
				},
				primaryKeyIDx: map[int]*entity.Pet{
					1: {
						ID:   1,
						Name: "Name 0",
					},
					2: {
						ID:   2,
						Name: "Name 1",
					},
					3: {
						ID:   3,
						Name: "Name 2",
					},
				},
				autoIncrementCount: 4,
			},
			args:    args{2},
			wantErr: false,
			want: []entity.Pet{
				{
					ID:   1,
					Name: "Name 0",
				},
				{
					ID:   2,
					Name: "Name 2",
				},
			},
		},
		{
			name: "second test",
			fields: fields{
				data: []*entity.Pet{
					{
						ID:   1,
						Name: "Name 0",
					},
					{
						ID:   2,
						Name: "Name 1",
					},
					{
						ID:   3,
						Name: "Name 2",
					},
				},
				primaryKeyIDx: map[int]*entity.Pet{
					1: {
						ID:   1,
						Name: "Name 0",
					},
					2: {
						ID:   2,
						Name: "Name 1",
					},
					3: {
						ID:   3,
						Name: "Name 2",
					},
				},
				autoIncrementCount: 4,
			},
			args:    args{5},
			wantErr: true,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			p := NewPetStorage()
			var err error
			p.data = tt.fields.data
			p.primaryKeyIDx = tt.fields.primaryKeyIDx
			p.autoIncrementCount = tt.fields.autoIncrementCount

			if err = p.Delete(tt.args.petID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}
			if err == nil {
				list := p.GetList()
				if fmt.Sprintf("%v", list) != fmt.Sprintf("%v", tt.want) {
					t.Errorf("GetList got = %v, want = %v", list, tt.want)
				}
			}
		})
	}
}
