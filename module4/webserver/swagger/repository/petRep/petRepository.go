package petRep

import (
	"fmt"
	"sync"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

type PetStorage struct {
	data               []*entity.Pet
	primaryKeyIDx      map[int]*entity.Pet
	autoIncrementCount int
	sync.Mutex
}

func NewPetStorage() *PetStorage {
	return &PetStorage{
		data:               make([]*entity.Pet, 0, 13),
		primaryKeyIDx:      make(map[int]*entity.Pet, 13),
		autoIncrementCount: 1,
	}
}

func (p *PetStorage) Create(pet entity.Pet) entity.Pet {
	p.Lock()
	defer p.Unlock()
	pet.ID = p.autoIncrementCount
	p.primaryKeyIDx[pet.ID] = &pet
	p.autoIncrementCount++
	p.data = append(p.data, &pet)
	return pet
}

func (p *PetStorage) Update(pet entity.Pet) (entity.Pet, error) {
	p.Lock()
	defer p.Unlock()

	if _, ok := p.primaryKeyIDx[pet.ID]; !ok {
		return entity.Pet{}, fmt.Errorf("not found")
	}
	p.primaryKeyIDx[pet.ID] = &pet
	p.data[pet.ID-1] = &pet
	return pet, nil
}

func (p *PetStorage) Delete(petID int) error {
	p.Lock()
	defer p.Unlock()

	if _, ok := p.primaryKeyIDx[petID]; !ok {
		return fmt.Errorf("not found")
	}
	p.data = append(p.data[:petID-1], p.data[petID:]...)
	newPrimaryKey := make(map[int]*entity.Pet, len(p.primaryKeyIDx))
	for i, pet := range p.data {
		pet.ID = i + 1
		newPrimaryKey[i+1] = pet
	}
	p.primaryKeyIDx = newPrimaryKey
	p.autoIncrementCount--

	return nil

}

func (p *PetStorage) GetByID(petID int) (entity.Pet, error) {

	if v, ok := p.primaryKeyIDx[petID]; ok {
		return *v, nil
	}
	return entity.Pet{}, fmt.Errorf("not found")
}

func (p *PetStorage) GetList() []entity.Pet {
	list := make([]entity.Pet, 0)
	for _, p := range p.data {
		list = append(list, *p)
	}
	return list
}
