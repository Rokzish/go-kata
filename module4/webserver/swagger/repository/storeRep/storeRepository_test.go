package storeRep

import (
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

func TestStoreStorage_Create(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		autoIncrementCount int
	}
	type args struct {
		order entity.Order
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   entity.Order
	}{
		{
			name: "first test",
			fields: fields{
				data:               make([]*entity.Order, 0, 5),
				primaryKeyIDx:      make(map[int]*entity.Order),
				autoIncrementCount: 1,
			},
			args: args{entity.Order{
				ID:       0,
				PetID:    14,
				Quantity: 2,
				ShipDate: "2023-05-12T14:00:39.942+0000",
				Status:   "available",
				Complete: false,
			}},
			want: entity.Order{
				ID:       1,
				PetID:    14,
				Quantity: 2,
				ShipDate: "2023-05-12T14:00:39.942+0000",
				Status:   "available",
				Complete: false,
			},
		},
		{
			name: "second test",
			fields: fields{
				data: []*entity.Order{
					{
						ID:       1,
						PetID:    2,
						Quantity: 10,
						ShipDate: "2023-03-01T14:00:00.000+0000",
						Status:   "available",
						Complete: false,
					},
				},
				primaryKeyIDx: map[int]*entity.Order{
					1: {
						ID:       1,
						PetID:    2,
						Quantity: 10,
						ShipDate: "2023-03-01T14:00:00.000+0000",
						Status:   "available",
						Complete: false,
					},
				},
				autoIncrementCount: 2,
			},
			args: args{entity.Order{
				ID:       0,
				PetID:    14,
				Quantity: 2,
				ShipDate: "2023-05-12T14:00:39.942+0000",
				Status:   "available",
				Complete: false,
			}},
			want: entity.Order{
				ID:       2,
				PetID:    14,
				Quantity: 2,
				ShipDate: "2023-05-12T14:00:39.942+0000",
				Status:   "available",
				Complete: false,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewStoreStorage()
			s.data = tt.fields.data
			s.primaryKeyIDx = tt.fields.primaryKeyIDx
			s.autoIncrementCount = tt.fields.autoIncrementCount
			if got := s.Create(tt.args.order); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Create() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_Delete(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		autoIncrementCount int
	}
	type args struct {
		orderID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
		want    []entity.Order
	}{
		{
			name: "first test",
			fields: fields{
				data: []*entity.Order{
					{
						ID:       1,
						PetID:    10,
						Quantity: 5,
						ShipDate: "",
						Status:   "some status 1",
						Complete: false,
					},
					{
						ID:       2,
						PetID:    12,
						Quantity: 7,
						ShipDate: "",
						Status:   "some status 2",
						Complete: false,
					},
				},
				primaryKeyIDx: map[int]*entity.Order{
					1: {
						ID:       1,
						PetID:    10,
						Quantity: 5,
						ShipDate: "",
						Status:   "some status 1",
						Complete: false,
					},
					2: {
						ID:       2,
						PetID:    12,
						Quantity: 7,
						ShipDate: "",
						Status:   "some status 2",
						Complete: false,
					},
				},
				autoIncrementCount: 3,
			},
			args:    args{1},
			wantErr: false,
			want: []entity.Order{
				{
					ID:       1,
					PetID:    12,
					Quantity: 7,
					ShipDate: "",
					Status:   "some status 2",
					Complete: false,
				},
			},
		},
		{
			name:    "second test",
			fields:  fields{},
			args:    args{2},
			wantErr: true,
			want:    nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewStoreStorage()
			s.data = tt.fields.data
			s.primaryKeyIDx = tt.fields.primaryKeyIDx
			s.autoIncrementCount = tt.fields.autoIncrementCount
			var err error

			if err = s.Delete(tt.args.orderID); (err != nil) != tt.wantErr {
				t.Errorf("Delete() error = %v, wantErr %v", err, tt.wantErr)
			}

			if err == nil {
				list := GetList(s)
				if fmt.Sprintf("%v", list) != fmt.Sprintf("%v", tt.want) {
					t.Errorf("GetList got = %v, want = %v", list, tt.want)
				}
			}
		})
	}
}

func TestStoreStorage_GetByID(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		autoIncrementCount int
	}
	type args struct {
		orderID int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    entity.Order
		wantErr bool
	}{
		{
			name: "first test",
			fields: fields{
				data:               nil,
				primaryKeyIDx:      nil,
				autoIncrementCount: 1,
			},
			args:    args{1},
			want:    entity.Order{},
			wantErr: true,
		},
		{
			name: "second test",
			fields: fields{
				data: []*entity.Order{
					{
						ID:       1,
						PetID:    1,
						Quantity: 1,
						ShipDate: "1",
						Status:   "1",
						Complete: false,
					},
					{
						ID:       2,
						PetID:    2,
						Quantity: 2,
						ShipDate: "2",
						Status:   "2",
						Complete: true,
					},
				},
				primaryKeyIDx: map[int]*entity.Order{
					1: {
						ID:       1,
						PetID:    1,
						Quantity: 1,
						ShipDate: "1",
						Status:   "1",
						Complete: false,
					},
					2: {
						ID:       2,
						PetID:    2,
						Quantity: 2,
						ShipDate: "2",
						Status:   "2",
						Complete: true,
					},
				},
				autoIncrementCount: 3,
			},
			args: args{2},
			want: entity.Order{
				ID:       2,
				PetID:    2,
				Quantity: 2,
				ShipDate: "2",
				Status:   "2",
				Complete: true,
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewStoreStorage()
			s.data = tt.fields.data
			s.primaryKeyIDx = tt.fields.primaryKeyIDx
			s.autoIncrementCount = tt.fields.autoIncrementCount
			var got entity.Order
			var err error
			got, err = s.GetByID(tt.args.orderID)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByID() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByID() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestStoreStorage_GetStoreStatuses(t *testing.T) {
	type fields struct {
		data               []*entity.Order
		primaryKeyIDx      map[int]*entity.Order
		autoIncrementCount int
	}
	tests := []struct {
		name   string
		fields fields
		want   map[string]int
	}{
		{
			name: "test 1",
			fields: fields{
				data: []*entity.Order{
					{
						ID:       1,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #1",
						Complete: false,
					},
					{
						ID:       2,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #2",
						Complete: false,
					},
					{
						ID:       3,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #3",
						Complete: false,
					},
					{
						ID:       4,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #2",
						Complete: false,
					},
					{
						ID:       5,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status 3",
						Complete: false,
					},
				},
				primaryKeyIDx: map[int]*entity.Order{
					1: {
						ID:       1,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #1",
						Complete: false,
					},
					2: {
						ID:       2,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #2",
						Complete: false,
					},
					3: {
						ID:       3,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #3",
						Complete: false,
					},
					4: {
						ID:       4,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status #2",
						Complete: false,
					},
					5: {
						ID:       5,
						PetID:    0,
						Quantity: 0,
						ShipDate: "",
						Status:   "status 3",
						Complete: false,
					},
				},
				autoIncrementCount: 6,
			},
			want: map[string]int{
				"status #1": 1,
				"status #2": 2,
				"status #3": 1,
				"status 3":  1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			s := NewStoreStorage()
			s.data = tt.fields.data
			s.primaryKeyIDx = tt.fields.primaryKeyIDx
			s.autoIncrementCount = tt.fields.autoIncrementCount
			var got map[string]int
			if got = s.GetStoreStatuses(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStoreStatuses() = %v, want %v", got, tt.want)
			}

		})
	}
}

func GetList(s *StoreStorage) []entity.Order {
	list := make([]entity.Order, 0)
	for _, o := range s.data {
		list = append(list, *o)
	}
	return list
}
