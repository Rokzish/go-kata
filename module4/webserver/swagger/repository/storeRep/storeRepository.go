package storeRep

import (
	"fmt"
	"sync"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

type StoreStorage struct {
	data               []*entity.Order
	primaryKeyIDx      map[int]*entity.Order
	autoIncrementCount int
	sync.Mutex
}

func NewStoreStorage() *StoreStorage {
	return &StoreStorage{
		data:               make([]*entity.Order, 0, 10),
		primaryKeyIDx:      make(map[int]*entity.Order, 10),
		autoIncrementCount: 1,
		Mutex:              sync.Mutex{},
	}
}

func (s *StoreStorage) Create(order entity.Order) entity.Order {
	s.Lock()
	defer s.Unlock()

	order.ID = s.autoIncrementCount
	s.primaryKeyIDx[order.ID] = &order
	s.autoIncrementCount++
	s.data = append(s.data, &order)
	return order
}

func (s *StoreStorage) GetByID(orderID int) (entity.Order, error) {
	if o, ok := s.primaryKeyIDx[orderID]; ok {
		return *o, nil
	}
	return entity.Order{}, fmt.Errorf("not found")
}

func (s *StoreStorage) Delete(orderID int) error {
	s.Lock()
	defer s.Unlock()

	if _, ok := s.primaryKeyIDx[orderID]; !ok {
		return fmt.Errorf("not found")
	}

	s.data = append(s.data[:orderID-1], s.data[orderID:]...)
	newPrimaryKey := make(map[int]*entity.Order, len(s.primaryKeyIDx))
	for i, o := range s.data {
		o.ID = i + 1
		newPrimaryKey[i+1] = o
	}
	s.primaryKeyIDx = newPrimaryKey
	s.autoIncrementCount--

	return nil
}

func (s *StoreStorage) GetStoreStatuses() map[string]int {
	list := make(map[string]int)
	for _, o := range s.data {
		list[o.Status]++
	}
	return list
}
