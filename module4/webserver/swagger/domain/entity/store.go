package entity

import "encoding/json"

func UnmarshalStore(data []byte) (Order, error) {
	var r Order
	err := json.Unmarshal(data, &r)
	return r, err
}

func (r *Order) Marshal() ([]byte, error) {
	return json.Marshal(r)
}

type Order struct {
	ID       int    `json:"id"`
	PetID    int64  `json:"petId"`
	Quantity int64  `json:"quantity"`
	ShipDate string `json:"shipDate"`
	Status   string `json:"status"`
	Complete bool   `json:"complete"`
}
