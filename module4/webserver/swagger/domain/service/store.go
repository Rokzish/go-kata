package service

import "gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"

type StoreStorage interface {
	Create(order entity.Order) entity.Order
	GetByID(orderID int) (entity.Order, error)
	Delete(orderID int) error
	GetStoreStatuses() map[string]int
}

type storeController struct {
	storage StoreStorage
}

func NewStoreStorage(storage StoreStorage) *storeController {
	return &storeController{storage: storage}
}

func (s *storeController) CreateOrder(order entity.Order) entity.Order {
	return s.storage.Create(order)
}

func (s *storeController) GetOrderByID(orderID int) (entity.Order, error) {
	return s.storage.GetByID(orderID)
}

func (s *storeController) DeleteOrder(orderID int) error {
	return s.storage.Delete(orderID)
}

func (s *storeController) GetStoreStatus() map[string]int {
	return s.storage.GetStoreStatuses()
}
