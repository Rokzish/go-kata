package service

import "gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"

type userStorage interface {
	Create(user entity.User)
	GetUserByName(username string) (entity.User, error)
	UpdateUserByName(username string, user entity.User) error
	DeleteUserByName(username string) error
}

type userController struct {
	storage userStorage
}

func NewUserController(storage userStorage) *userController {
	return &userController{storage: storage}
}

func (u *userController) CreateUser(user entity.User) {
	u.storage.Create(user)
}

func (u *userController) GetUserByName(username string) (entity.User, error) {
	return u.storage.GetUserByName(username)
}

func (u *userController) UpdateUserByName(username string, user entity.User) error {
	return u.storage.UpdateUserByName(username, user)
}

func (u *userController) DeleteUserByName(username string) error {
	return u.storage.DeleteUserByName(username)
}
