package service

import "gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"

type petStorage interface {
	Create(pet entity.Pet) entity.Pet
	Update(pet entity.Pet) (entity.Pet, error)
	Delete(petID int) error
	GetByID(petID int) (entity.Pet, error)
	GetList() []entity.Pet
}

type petController struct {
	storage petStorage
}

func NewPetController(storage petStorage) *petController {
	return &petController{storage: storage}
}

func (p *petController) CreatePet(pet entity.Pet) entity.Pet {
	return p.storage.Create(pet)
}

func (p *petController) UpdatePet(pet entity.Pet) (entity.Pet, error) {
	return p.storage.Update(pet)
}

func (p *petController) DeletePet(petID int) error {
	return p.storage.Delete(petID)
}

func (p *petController) GetPetByID(petID int) (entity.Pet, error) {
	return p.storage.GetByID(petID)
}

func (p *petController) GetPetList() []entity.Pet {
	return p.storage.GetList()
}
