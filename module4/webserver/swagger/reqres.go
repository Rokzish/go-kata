package main

import (
	"bytes"

	"gitlab.com/Rokzish/go-kata/module4/webserver/swagger/domain/entity"
)

func init() {
	_ = petAddRequest{}
	_ = petAddResponse{}
	_ = petDeleteRequest{}
	_ = petUpdateRequest{}
	_ = storeCreateResponse{}
	_ = errInputResponse{}
	_ = errNotFoundResponse{}
	_ = storeDeleteByIDRequest{}
	_ = petGetByIDResponse{}
	_ = petGetByIDRequest{}
	_ = userCreateRequest{}
	_ = storeGetByIDRequest{}
	_ = petUpdateFormResponse{}
	_ = petUploadImgResponse{}
	_ = storeGetByIDResponse{}
	_ = petFindByStatusResponse{}
	_ = userUpdateByNameRequest{}
	_ = storeCreateRequest{}
	_ = petFindByStatusRequest{}
	_ = storeGetInventoryResponse{}
	_ = userGetByNameRequest{}
	_ = userGetByNameResponse{}
	_ = petUpdateResponse{}
	_ = petUploadImgRequest{}
	_ = userCreateListRequest{}
	_ = userDeleteRequest{}
	_ = petUpdateRequest{}
	_ = petUpdateFormRequest{}
}

// swagger:route POST /pet pet petAddRequest
// Добавление питомца.
// responses:
// 	200: petAddResponse
//  400: errInputResponse

// swagger:parameters petAddRequest
type petAddRequest struct {
	// in:body
	// required:true
	Body entity.Pet
}

// swagger:response petAddResponse
type petAddResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route GET /pet/{id} pet petGetByIDRequest
// Получение питомца по id.
// responses:
//   200: petGetByIDResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters petGetByIDRequest
type petGetByIDRequest struct {
	// ID of pet
	//
	// In: path
	// required:true
	ID string `json:"id"`
}

// swagger:response petGetByIDResponse
type petGetByIDResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route DELETE /pet/{id} pet petDeleteRequest
// Удаление питомца по id.
// responses:
//
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters petDeleteRequest
type petDeleteRequest struct {
	// ID of pet
	//
	// In: path
	ID string `json:"id"`
}

// swagger:route PUT /pet pet petUpdateRequest
// Обновление информации о питомце.
// responses:
// 	 200: petUpdateResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters petUpdateRequest
type petUpdateRequest struct {
	// in:body
	// required:true
	Body entity.Pet
}

// swagger:response petUpdateResponse
type petUpdateResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route GET /pet/findByStatus pet petFindByStatusRequest
// Поиск питомцев по статусу.
// responses:
// 	 200: petFindByStatusResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters petFindByStatusRequest
type petFindByStatusRequest struct {
	// Значения статуса, которые необходимо учитывать для фильтрации.
	// in:query
	// type:string
	// required:true
	// explode:true
	// default:available
	// Enum:available,pending,sold
	Status string
}

// swagger:response petFindByStatusResponse
type petFindByStatusResponse struct {
	// in:body
	Body []entity.Pet
}

// swagger:route POST /pet/{petID} pet petUpdateFormRequest
// Обновляет питомца данными формы.
// responses:
// 	 200: petUpdateFormResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters petUpdateFormRequest
type petUpdateFormRequest struct {
	// ID питомца, которого нужно обновить.
	// in:path
	// required:true
	// type:int
	ID int `json:"petID"`

	// Имя питомца, которое нужно изменить.
	// in:query
	// type:string
	Name string `json:"name"`

	// Статус питомца, который нужно изменить.
	// in:query
	// type:string
	Status string `json:"status"`
}

// swagger:response petUpdateFormResponse
type petUpdateFormResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route POST /pet/{petID}/uploadImage pet petUploadImgRequest
// Обновляет питомца данными формы.
// responses:
// 	 200: petUploadImgResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters petUploadImgRequest
type petUploadImgRequest struct {
	// ID питомца для загрузки.
	// in:path
	// required:true
	// type:int
	ID int `json:"petID"`

	// formFile
	// in: formData
	// required:true
	// swagger:file
	FormFile *bytes.Buffer `json:"formFile"`
}

// swagger:response petUploadImgResponse
type petUploadImgResponse struct {
	// in:body
	Body entity.Pet
}

// swagger:route POST /store/order store storeCreateRequest
// Разместить заказ по питомцу.
// responses:
// 	 200: storeCreateResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters storeCreateRequest
type storeCreateRequest struct {
	// Размещение заказа на покупу питомца.
	//
	// in:body
	// required:true
	Body entity.Order
}

// swagger:response storeCreateResponse
type storeCreateResponse struct {
	// in:body
	Body entity.Order
}

// swagger:route GET /store/order/{orderID} store storeGetByIDRequest
// Найти покупку по ID заказа.
// responses:
// 	 200: storeGetByIDResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters storeGetByIDRequest
type storeGetByIDRequest struct {
	// ID заказа, которого нужно получить.
	// in:path
	// required:true
	ID int `json:"orderID"`
}

// swagger:response storeGetByIDResponse
type storeGetByIDResponse struct {
	// in:body
	Body entity.Order
}

// swagger:route DELETE /store/order/{orderID} store storeDeleteByIDRequest
// Удаление заказ по ID.
// responses:
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters storeDeleteByIDRequest
type storeDeleteByIDRequest struct {
	// ID заказа, которого нужно удалить.
	// in:path
	// required:true
	ID int `json:"orderID"`
}

// swagger:route GET /store/inventory store store
// Возвращает карту статусов заказов и их количества.
// responses:
//	 200: storeGetInventoryResponse

// swagger:response storeGetInventoryResponse
type storeGetInventoryResponse struct {
	//in:body
	// type:object
	Body map[string]int
}

// swagger:route POST /user/createWithList user userCreateListRequest
// Создать список пользователей из переданного массива.
// responses:
//   400: errInputResponse

// swagger:parameters userCreateListRequest
type userCreateListRequest struct {
	// Список объектов пользователей.
	// in:body
	// required:true
	Body []entity.User
}

// swagger:route GET /user/{username} user userGetByNameRequest
// Найти пользователя по username.
// responses:
// 	 200: userGetByNameResponse
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters userGetByNameRequest
type userGetByNameRequest struct {
	// Имя пользователя для поиска.
	// in:path
	// required:true
	Username string `json:"username"`
}

// swagger:response userGetByNameResponse
type userGetByNameResponse struct {
	// in:body
	Body entity.User
}

// swagger:route PUT /user/{username} user userUpdateByNameRequest
// Обновить информацию о пользователе.
// responses:
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters userUpdateByNameRequest
type userUpdateByNameRequest struct {
	// Имя пользователя для поиска.
	// in:path
	// required:true
	Username string `json:"username"`

	// Информация о пользователе, для обновления.
	// in:body
	// required:true
	Body entity.User
}

// swagger:route DELETE /user/{username} user userDeleteRequest
// Удаление пользователя.
// responses:
//   400: errInputResponse
//   404: errNotFoundResponse

// swagger:parameters userDeleteRequest
type userDeleteRequest struct {
	// Имя пользователя для удаления.
	// in:path
	// required:true
	Username string `json:"username"`
}

// swagger:route POST /user user userCreateRequest
// Создание нового пользователя.
// responses:
//   400: errInputResponse

// swagger:parameters userCreateRequest
type userCreateRequest struct {
	// Создание объекта пользователя.
	// in:body
	// required:true
	Body entity.User
}

// Not found
// swagger:response errNotFoundResponse
type errNotFoundResponse struct {
}

// Invalid input
// swagger:response errInputResponse
type errInputResponse struct {
}
