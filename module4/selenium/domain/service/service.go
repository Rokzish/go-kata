package service

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"

	"github.com/tebeka/selenium"
	"gitlab.com/Rokzish/go-kata/module4/selenium/domain/entity"
)

const (
	maxTries         = 15
	HabrCareerLink   = "https://career.habr.com"
	countPageVacancy = 25
)

type Storage interface {
	Create(dto entity.Vacancy)
	GetByID(id int) (entity.Vacancy, error)
	GetList() ([]entity.Vacancy, error)
	Delete(id int) error
}

type VacancyController struct {
	storage Storage
	wd      selenium.WebDriver
}

func NewVacancyController(storage Storage, wd selenium.WebDriver) *VacancyController {
	return &VacancyController{storage: storage, wd: wd}
}

// UpdateVacancyList update vacancy list in storage by query
func (v *VacancyController) UpdateVacancyList(query string) (int, error) {
	var vacancyCount int
	var err error
	for i := 0; i < maxTries; {
		vacancyCount, err = getVacancyCount(v.wd, query)
		if err != nil {
			log.Println(err)
			i++
			continue
		}
		break
	}

	pages := vacancyCount / countPageVacancy
	isDiv := vacancyCount%countPageVacancy == 0
	if !isDiv {
		pages++
	}
	links := make([]string, 0)

	for i := 1; i <= pages; i++ {
		for j := 0; j < maxTries; {
			link, err := getLinksFromPage(v.wd, i, query)
			if err != nil {
				log.Println(err)
				j++
				continue
			}
			links = append(links, link...)
			break

		}
	}
	var availableVacancy int
	var vacancies []entity.Vacancy
	for _, link := range links {
		vac, err := getDataFromLink(link)
		if err != nil {
			continue
		}
		fmt.Println(link)
		availableVacancy++
		vacancies = append(vacancies, vac)
	}

	for _, vac := range vacancies {
		v.storage.Create(vac)
	}
	return availableVacancy, nil
}

func getVacancyCount(wd selenium.WebDriver, query string) (int, error) {
	if err := wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=1&q=%s&type=all", query)); err != nil {
		return 0, err
	}

	elem, err := wd.FindElement(selenium.ByCSSSelector, ".search-total")
	if err != nil {
		return 0, err
	}
	vacancyCountRaw, err := elem.Text()
	if err != nil {
		return 0, err
	}

	vacancyCountSlice := strings.Split(vacancyCountRaw, " ")
	count, err := strconv.Atoi(vacancyCountSlice[1])
	if err != nil {
		return 0, nil
	}

	return count, nil
}

func getLinksFromPage(wd selenium.WebDriver, page int, query string) ([]string, error) {
	if err := wd.Get(fmt.Sprintf("https://career.habr.com/vacancies?page=%d&q=%s&type=all", page, query)); err != nil {
		return nil, err
	}

	elems, err := wd.FindElements(selenium.ByCSSSelector, ".vacancy-card__title-link")
	if err != nil {
		return nil, err
	}
	var links []string
	for i := range elems {
		var link string
		link, err = elems[i].GetAttribute("href")
		if err != nil {
			log.Println(err)
			continue
		}
		links = append(links, HabrCareerLink+link)

	}

	return links, nil
}

func getDataFromLink(link string) (entity.Vacancy, error) {

	resp, err := http.Get(link)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode == http.StatusNotFound {
		return nil, fmt.Errorf("link content not available")
	}

	var doc *goquery.Document
	doc, err = goquery.NewDocumentFromReader(resp.Body)
	if err != nil && doc != nil {
		return nil, err
	}

	dd := doc.Find("script[type=\"application/ld+json\"]")
	if dd == nil {
		return nil, fmt.Errorf("habr vacancy nodes not found")
	}
	ss := dd.First().Text()

	var vacancy entity.Vacancy
	err = json.Unmarshal([]byte(ss), &vacancy)
	if err != nil {
		return nil, err
	}
	return vacancy, nil

}

func (v *VacancyController) DeleteVacancy(id int) error {
	return v.storage.Delete(id)
}

func (v *VacancyController) GetVacancyByID(id int) (entity.Vacancy, error) {
	return v.storage.GetByID(id)
}

func (v *VacancyController) GetList() ([]entity.Vacancy, error) {
	return v.storage.GetList()
}
