package handler

import (
	"encoding/json"
	"net/http"

	"gitlab.com/Rokzish/go-kata/module4/selenium/domain/entity"
)

type VacancyService interface {
	UpdateVacancyList(query string) (int, error)
	DeleteVacancy(id int) error
	GetVacancyByID(id int) (entity.Vacancy, error)
	GetList() ([]entity.Vacancy, error)
}

type VacancyHandler struct {
	service VacancyService
}

func NewVacancyHandler(service VacancyService) *VacancyHandler {
	return &VacancyHandler{service: service}
}

func (v *VacancyHandler) SearchVacancyByQuery(w http.ResponseWriter, r *http.Request) {
	var (
		query        ReqBodyQuery
		err          error
		vacancyCount ResBodyVacCount
	)
	err = json.NewDecoder(r.Body).Decode(&query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vacancyCount.Count, err = v.service.UpdateVacancyList(query.Query)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json;charset=utf-8") // выставляем заголовки, что отправляем json в utf8
	err = json.NewEncoder(w).Encode(vacancyCount)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyHandler) DeleteVacancyByID(w http.ResponseWriter, r *http.Request) {
	var (
		err error
		id  ReqBodyID
	)
	err = json.NewDecoder(r.Body).Decode(&id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	err = v.service.DeleteVacancy(id.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode("Deletion successful")

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}

func (v *VacancyHandler) GetVacancyByID(w http.ResponseWriter, r *http.Request) {
	var (
		vac entity.Vacancy
		err error
		id  ReqBodyID
	)
	err = json.NewDecoder(r.Body).Decode(&id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	vac, err = v.service.GetVacancyByID(id.ID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err = json.NewEncoder(w).Encode(vac)

	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func (v *VacancyHandler) GetListVacancy(w http.ResponseWriter, r *http.Request) {
	var (
		vacancies []entity.Vacancy
		err       error
	)
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	vacancies, err = v.service.GetList()
	if err != nil {
		err := json.NewEncoder(w).Encode("List is empty")
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	err = json.NewEncoder(w).Encode(vacancies)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

}
